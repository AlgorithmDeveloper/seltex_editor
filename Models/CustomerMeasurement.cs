﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class CustomerMeasurement : BaseModel //CustomerMeasurement //   // 
    {
        private int cstmrMeasureID; 
        public int CstmrMeasureID
        {
            get { return cstmrMeasureID; }
            set
            {
                if (Equals(value, cstmrMeasureID)) return;
                cstmrMeasureID = value;
                NotifyPropertyChanged();
            }
        }

        private string cstmrMeasureName;
        public string CstmrMeasureName
        {
            get { return cstmrMeasureName; }
            set
            {
                if (Equals(value, cstmrMeasureName)) return;
                cstmrMeasureName = value;
                NotifyPropertyChanged();
            }
        }

        private string cstmrMeasureValue;
        public string CstmrMeasureValue
        {
            get { return cstmrMeasureValue; }
            set
            {
                if (Equals(value, cstmrMeasureValue)) return;
                cstmrMeasureValue = value;
                NotifyPropertyChanged();
            }
        }
       

        public CustomerMeasurement(CustomerMeasurement previousCstmrer)
        {
            CstmrMeasureID = previousCstmrer.CstmrMeasureID;
            CstmrMeasureName = String.IsNullOrEmpty(previousCstmrer.CstmrMeasureName) ? string.Empty : previousCstmrer.CstmrMeasureName;
            CstmrMeasureValue = String.IsNullOrEmpty(previousCstmrer.CstmrMeasureValue) ? string.Empty : previousCstmrer.CstmrMeasureValue;            
        }
        public CustomerMeasurement(int _CstmrMeasureID, string _CstmrMeasureName, string _CstmrMeasureValue)
        {
            CstmrMeasureID = _CstmrMeasureID;
            CstmrMeasureName = String.IsNullOrEmpty(_CstmrMeasureName) ? string.Empty : _CstmrMeasureName;
            CstmrMeasureValue = String.IsNullOrEmpty(_CstmrMeasureValue) ? string.Empty : _CstmrMeasureValue;
        }
        public CustomerMeasurement() { }
    }
    public class CustomersMeasurement : ObservableCollection<CustomerMeasurement>
    {
        // Creating the collection in this way enables data binding from XAML.
    }
}
