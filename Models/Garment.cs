﻿using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Models
{
    public class Garment : GarmentDetails
    {
        private string garmentDescription;
        [XmlAttribute("Description")]
        public string GarmentDescription
        {
            get { return garmentDescription; }
            set
            {
                if (Equals(value, garmentDescription)) return;
                garmentDescription = value;
                NotifyPropertyChanged();
            }
        }

        private DateTime garmentDefRuleDate;
        [XmlAttribute("Time_Define")]
        public DateTime GarmentDefRuleDate
        {
            get { return garmentDefRuleDate; }
            set
            {
                if (Equals(value, garmentDefRuleDate)) return;
                garmentDefRuleDate = value;
                NotifyPropertyChanged();
            }
        }

        private Status garmentStage;
        [XmlAttribute("Status")]
        public Status GarmentStage
        {
            get { return garmentStage; }
            set
            {
                if (Equals(value, garmentStage)) return;
                garmentStage = value;
                NotifyPropertyChanged();
            }
        }

        private Measurements measureCollection = null;
        [XmlElement("Measurements_List")]
        public Measurements MeasureCollection
        {
            get
            {
                if (measureCollection == null)
                    measureCollection = new Measurements();
                return measureCollection;
            }
            set
            {
                if (Equals(value, measureCollection)) return;
                measureCollection = value;
                NotifyPropertyChanged();
            }
        }

        public Garment(Garment previousGarment) : base(previousGarment)
        {            
            GarmentDescription = String.IsNullOrEmpty(previousGarment.GarmentDescription) ? string.Empty : previousGarment.GarmentDescription;
            GarmentDefRuleDate = previousGarment.GarmentDefRuleDate;
            GarmentStage = previousGarment.GarmentStage;
            MeasureCollection = DeepCopyMeasures(previousGarment.MeasureCollection);
        }
        
        public Garment(int _garmentID, string _garmentName, string _garmentDescription = "", Measurements _measureCollection = null, DateTime? _styleDate = null) : base(_garmentID, _garmentName)
        {
            GarmentDescription = String.IsNullOrEmpty(_garmentDescription) ? string.Empty : _garmentDescription;

            //The ?? operator is called the null - coalescing operator. It returns the left-hand operand if the operand is not null; otherwise it returns the right hand operand.
            GarmentDefRuleDate = _styleDate ?? DateTime.Now;

            //   String.IsNullOrEmpty(...) -> true if the value parameter is null or an empty string(""); otherwise, false.
            //  evaluate to true or false.If condition is true, first_expression .If condition is false, second_expression

            GarmentStage = Status.review;
            if (_measureCollection != null)
                MeasureCollection = DeepCopyMeasures(_measureCollection);
        }

        public Garment()
        {
            GarmentDefRuleDate = DateTime.Now;
            GarmentStage = Status.review;
        }
        public Garment(GarmentDetails previousGarment) : base(previousGarment)
        {
            GarmentDescription = string.Empty;
            GarmentDefRuleDate = DateTime.Now;
            GarmentStage = Status.review;
        }
    }
    public class Garments : ObservableCollection<Garment>
    {

        // Creating the collection in this way enables data binding from XAML.
    }
}
