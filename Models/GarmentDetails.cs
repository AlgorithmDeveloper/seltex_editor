﻿using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Models
{
    public class GarmentDetails : BaseModel
    {
        private int garmentID;
        [XmlIgnore]
        public int GarmentID
        {
            get { return garmentID; }
            set
            {
                if (Equals(value, garmentID)) return;
                garmentID = value;
                NotifyPropertyChanged();
            }
        }

        private string garmentName;
        [XmlAttribute("Name")]
        public string GarmentName
        {
            get { return garmentName; }
            set
            {
                if (Equals(value, garmentName)) return;
                garmentName = value;
                NotifyPropertyChanged();
            }
        }
        
        public GarmentDetails(GarmentDetails previousGarmentDetails)
        {
            GarmentID = previousGarmentDetails.GarmentID;
            GarmentName = previousGarmentDetails.GarmentName;           
        }

        public GarmentDetails(int _garmentID, string _garmentName)
        {
            GarmentID = _garmentID;
            GarmentName = _garmentName;           
        }        

        public GarmentDetails() { }

        protected static Measurements DeepCopyMeasures(Measurements collection)
        {
            Measurements newCollection = new Measurements();

            foreach (Measurement item in collection)
            {
                Measurement newItem = new Measurement(item);
                newCollection.Add(newItem);
            }
            return newCollection;
        }

        protected static RulesLibraries DeepCopyRules(RulesLibraries collection)
        {
            RulesLibraries newCollection = new RulesLibraries();

            foreach (RuleLibrary item in collection)
            {
                RuleLibrary newItem = new RuleLibrary(item);
                newCollection.Add(newItem);
            }
            return newCollection;
        }
    }
    public class GarmentsDetails : ObservableCollection<GarmentDetails>
    {
        // Creating the collection in this way enables data binding from XAML.
    }
}
