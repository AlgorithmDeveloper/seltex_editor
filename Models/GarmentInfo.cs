﻿using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Models
{
    public class GarmentInfo : GarmentDetails
    {
        private string garmentPath;
        [XmlElement("Style_Path")]
        public string GarmentPath
        {
            get { return garmentPath; }
            set
            {
                if (Equals(value, garmentPath)) return;
                garmentPath = value;
                NotifyPropertyChanged();
            }
        }

        private string dmcPath;
        [XmlElement("DMC_Path")]
        public string DMCPath
        {
            get { return dmcPath; }
            set
            {
                if (Equals(value, dmcPath)) return;
                dmcPath = value;
                NotifyPropertyChanged();
            }
        }

        private string rulePath;
        [XmlElement("Rule_Path")]
        public string RulePath
        {
            get { return rulePath; }
            set
            {
                if (Equals(value, rulePath)) return;
                rulePath = value;
                NotifyPropertyChanged();
            }
        }

        private Measurements measureCollection = null;
        [XmlElement("Measurements_List")]
        public Measurements MeasureCollection
        {
            get
            {
                if (measureCollection == null)
                    measureCollection = new Measurements();
                return measureCollection;
            }
            set
            {
                if (Equals(value, measureCollection)) return;
                measureCollection = value;
                NotifyPropertyChanged();
            }
        }

        private RulesLibraries ruleCollection = null;
        [XmlElement("Rules_List")]
        public RulesLibraries RuleCollection
        {
            get
            {
                if (ruleCollection == null)
                    ruleCollection = new RulesLibraries();
                return ruleCollection;
            }
            set
            {
                if (Equals(value, ruleCollection)) return;
                ruleCollection = value;
                NotifyPropertyChanged();
            }
        }

        public GarmentInfo(GarmentInfo previousGarmentInfo) : base(previousGarmentInfo)
        {
            GarmentPath = previousGarmentInfo.GarmentPath;
            DMCPath = previousGarmentInfo.DMCPath;
            RulePath = previousGarmentInfo.RulePath;
            MeasureCollection = DeepCopyMeasures(previousGarmentInfo.MeasureCollection);
            RuleCollection = DeepCopyRules(previousGarmentInfo.RuleCollection);
        }

        public GarmentInfo(int _garmentID, string _garmentName, string _garmentPath = "", string _DMCPath = "", string _rulePath = "",
                           Measurements _measureCollection = null, RulesLibraries _ruleCollection = null) : base(_garmentID, _garmentName)
        {
            GarmentPath = _garmentPath;
            DMCPath = _DMCPath;
            RulePath = _rulePath;
            if (_ruleCollection != null)
                RuleCollection = DeepCopyRules(_ruleCollection);
            if (_measureCollection != null)
                MeasureCollection = DeepCopyMeasures(_measureCollection);
        }

        public GarmentInfo() { }
    }
    public class GarmentsInfo : ObservableCollection<GarmentInfo>
    {
        // Creating the collection in this way enables data binding from XAML.
    }


}

