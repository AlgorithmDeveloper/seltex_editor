﻿using System;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Models
{
    public class GradingRule : Vertex
    {
        private int gradeID;
        [XmlIgnore]
        public int GradeID
        {
            get { return gradeID; }
            set
            {
                if (Equals(value, gradeID)) return;
                gradeID = value;
                NotifyPropertyChanged();
            }
        }

        private Double gradeDX;
        [XmlElement("DX")]
        public Double GradeDX
        {
            get { return gradeDX; }
            set
            {
                if (Equals(value, gradeDX)) return;
                gradeDX = value;
                NotifyPropertyChanged();
            }
        }

        private Double gradeDY;
        [XmlElement("DY")]
        public Double GradeDY
        {
            get { return gradeDY; }
            set
            {
                if (Equals(value, gradeDY)) return;
                gradeDY = value;
                NotifyPropertyChanged();
            }
        }

        private Vertex vertexFirst;
        [XmlElement("First_Point", IsNullable = false)] // // [XmlIgnore]
        public Vertex VertexFirst
        {
            get
            {
                if (vertexFirst == null)
                    vertexFirst = new Vertex();
                return vertexFirst;
            }
            set
            {
                if (Equals(value, vertexFirst)) return;
                vertexFirst = value;
                NotifyPropertyChanged();
            }
        }

        private Vertex vertexLast;
        [XmlElement("Last_Point", IsNullable = true)] //   [XmlIgnore]
        public Vertex VertexLast
        {
            get
            {
                if (vertexLast == null)
                    vertexLast = new Vertex();
                return vertexLast;
            }
            set
            {
                if (Equals(value, vertexLast)) return;
                vertexLast = value;
                NotifyPropertyChanged();
            }
        }

        private Piece parentPiece;
        [XmlElement("Parent_Vertex")]  //  [XmlIgnore] 
        public Piece ParentPiece
        {
            get
            {
                if (parentPiece == null)
                    parentPiece = new Piece();
                return parentPiece;
            }
            set
            {
                if (Equals(value, parentPiece)) return;
                parentPiece = value;
                NotifyPropertyChanged();
            }
        }

        public GradingRule(GradingRule previousGradingRule) : base (previousGradingRule)
        {
            GradeID = previousGradingRule.GradeID;
            GradeDX = previousGradingRule.GradeDX;
            GradeDY = previousGradingRule.GradeDY;
            VertexFirst = new Vertex(previousGradingRule.VertexLast);
            VertexLast = new Vertex(previousGradingRule.VertexLast);
            ParentPiece = new Piece(previousGradingRule.ParentPiece);
        }

        public GradingRule(Vertex previousVertex) : base(previousVertex)
        {
            GradeID = 0;
            GradeDX = 0.0;
            GradeDY = 0.0;
            VertexFirst = new Vertex();
            VertexLast = new Vertex();
            ParentPiece = new Piece();
        }

        public GradingRule(int _gradeID, int _vertexID, string _vertexName, Double _gradeDX = 0.0D, Double _gradeDY = 0.0D,
            Vertex _vertexfirst = null, Vertex _vertexlast = null) : base(_vertexID, _vertexName)
        {            

            GradeID = _gradeID;
            GradeDX = _gradeDX;
            GradeDY = _gradeDY;
            if (_vertexfirst != null)
                VertexFirst = new Vertex(_vertexfirst);
            else
                VertexFirst = new Vertex();
            if (_vertexfirst != null)
                VertexLast = new Vertex(_vertexlast);
            else
                VertexLast = new Vertex();
        }

        public GradingRule() { }
    }
    public class GradingRules : ObservableCollection<GradingRule>
    {
        // Creating the collection in this way enables data binding from XAML.
    }
}
