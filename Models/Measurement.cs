﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Serialization;

namespace Models
{
    public class Measurement : BaseModel
    {
        private int measureID;
        [XmlIgnore]
        public int MeasureID
        {
            get { return measureID; }
            set
            {
                if (Equals(value, measureID)) return;
                measureID = value;
                NotifyPropertyChanged();
            }
        }

        private string measureName;
        [XmlAttribute("Name")]
        public string MeasureName
        {
            get => measureName; set
            {
                if (Equals(value, measureName)) return;
                measureName = value;
                NotifyPropertyChanged();
            }
        }

        [XmlAttribute("Has_Rule")]
        private bool measureIsDef;
        public bool MeasureIsDef
        {
            get => measureIsDef; set
            {
                if (Equals(value, measureIsDef)) return;
                measureIsDef = value;
                NotifyPropertyChanged();
            }
        }

        private string measureDescription;
        [XmlIgnore]
        public string MeasureDescription
        {
            get { return measureDescription; }
            set
            {
                if (Equals(value, measureDescription)) return;
                measureDescription = value;
                NotifyPropertyChanged();
            }
        }

        private GarmentDetails parentGarment;
        [XmlElement("Parent_Measure")]
        public GarmentDetails ParentGarment
        {
            get
            {
                if (parentGarment == null)
                    parentGarment = new GarmentDetails();
                return parentGarment;
            }
            set
            {
                if (Equals(value, parentGarment)) return;
                parentGarment = value;
                NotifyPropertyChanged();
            }
        }

        private Pieces pieceCollection = null;
        [XmlElement("Pieces_List")]
        public Pieces PieceCollection
        {
            get
            {
                if (pieceCollection == null)
                    pieceCollection = new Pieces();
                return pieceCollection;
            }
            set
            {
                if (Equals(value, pieceCollection)) return;
                pieceCollection = value;
                NotifyPropertyChanged();
            }
        }

        // Copy constructor.
        public Measurement(Measurement previousMeasurement)
        {
            measureID = previousMeasurement.MeasureID;
            measureName = previousMeasurement.MeasureName;
            MeasureIsDef = previousMeasurement.MeasureIsDef;
            MeasureDescription = String.IsNullOrEmpty(previousMeasurement.MeasureDescription) ? string.Empty : previousMeasurement.MeasureDescription;
            ParentGarment = new GarmentDetails(previousMeasurement.ParentGarment);
            PieceCollection = DeepCopy(previousMeasurement.PieceCollection);            
        }

        public Measurement(int _measureID, string _measureName, Pieces _pieceCollection = null, string _measureDescription = "")
        {
            measureID = _measureID;
            measureName = _measureName;
            MeasureIsDef = false;
            MeasureDescription = String.IsNullOrEmpty(_measureDescription) ? string.Empty : _measureDescription;
            if (_pieceCollection != null)
                PieceCollection = DeepCopy(_pieceCollection);                   
        }
        
        public Measurement() { }

        private static Pieces DeepCopy(Pieces collection)
        {
            Pieces newCollection = new Pieces();

            foreach (Piece item in collection)
            {
                Piece newItem = new Piece(item);
                newCollection.Add(newItem);
            }
            return newCollection;
        }

        private static ObservableCollection<T> DeepCopy1<T>(ObservableCollection<T> list) where T : ICloneable
        {

            return new ObservableCollection<T>(list.Select(x => x.Clone()).Cast<T>());

            // ObservableCollection<T> newList = new ObservableCollection<T>();
            //foreach (T rec in list)
            //{
            //    newList.Add((T)rec.Clone());
            //}
            //return newList;
        }

    }
    public class Measurements : ObservableCollection<Measurement>
    {
        // Creating the collection in this way enables data binding from XAML.
    }


}
