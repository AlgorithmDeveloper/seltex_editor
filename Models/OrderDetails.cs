﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class OrderDetails : BaseModel // Order  Order
    {
        private int orderID;       
        public int OrderID
        {
            get { return orderID; }
            set
            {
                if (Equals(value, orderID)) return;
                orderID = value;
                NotifyPropertyChanged();
            }
        }

        private string customerName; 
        public string CustomerName
        {
            get { return customerName; }
            set
            {
                if (Equals(value, customerName)) return;
                customerName = value;
                NotifyPropertyChanged();
            }
        }

        private GarmentInfo orderGarment;
        public GarmentInfo OrderGarment
        {
            get
            {
                if (orderGarment == null)
                    orderGarment = new GarmentInfo();
                return orderGarment;
            }
            set
            {
                if (Equals(value, orderGarment)) return;
                orderGarment = value;
                NotifyPropertyChanged();
            }
        }

        private string orderDescription;       
        public string OrderDescription
        {
            get { return orderDescription; }
            set
            {
                if (Equals(value, orderDescription)) return;
                orderDescription = value;
                NotifyPropertyChanged();
            }
        }

        private string orderUnits;
        public string OrderUnits
        {
            get { return orderUnits; }
            set
            {
                if (Equals(value, orderUnits)) return;
                orderUnits = value;
                NotifyPropertyChanged();
            }
        }

        private DateTime orderScanned;
        public DateTime OrderScanned
        {
            get { return orderScanned; }
            set
            {
                if (Equals(value, orderScanned)) return;
                orderScanned = value;
                NotifyPropertyChanged();
            }
        }

        private DateTime orderReceived;      
        public DateTime OrderReceived
        {
            get { return orderReceived; }
            set
            {
                if (Equals(value, orderReceived)) return;
                orderReceived = value;
                NotifyPropertyChanged();
            }
        }
        
        private Status orderStage; 
        public Status OrderStage
        {
            get { return orderStage; }
            set
            {
                if (Equals(value, orderStage)) return;
                orderStage = value;
                NotifyPropertyChanged();
            }
        }

        private string orderPath;       
        public string OrderPath
        {
            get { return orderPath; }
            set
            {
                if (Equals(value, orderPath)) return;
                orderPath = value;
                NotifyPropertyChanged();
            }
        }

        private string batchPath;
        public string BatchPath
        {
            get { return batchPath; }
            set
            {
                if (Equals(value, batchPath)) return;
                batchPath = value;
                NotifyPropertyChanged();
            }
        }

        private int orderBaseSize = -1;
        public int OrderBaseSize
        {
            get { return orderBaseSize; }
            set
            {
                if (Equals(value, orderBaseSize)) return;
                orderBaseSize = value;
                NotifyPropertyChanged();
            }
        }

        private string recommendSize;
        public string RecommendSize
        {
            get { return recommendSize; }
            set
            {
                if (Equals(value, recommendSize)) return;
                recommendSize = value;
                NotifyPropertyChanged();
            }
        }

        private CustomersMeasurement cstmrMeasureCollection = null;
        public CustomersMeasurement CstmrMeasureCollection
        {
            get
            {
                if (cstmrMeasureCollection == null)
                    cstmrMeasureCollection = new CustomersMeasurement();
                return cstmrMeasureCollection;
            }
            set
            {
                if (Equals(value, cstmrMeasureCollection)) return;
                cstmrMeasureCollection = value;
                NotifyPropertyChanged();
            }
        }

        private CustomersMeasurement cstmrMeasureSelectedCollec= null;
        public CustomersMeasurement CstmrMeasureSelectedCollec
        {
            get
            {
                if (cstmrMeasureSelectedCollec == null)
                    cstmrMeasureSelectedCollec = new CustomersMeasurement();
                return cstmrMeasureSelectedCollec;
            }
            set
            {
                if (Equals(value, cstmrMeasureSelectedCollec)) return;
                cstmrMeasureSelectedCollec = value;
                NotifyPropertyChanged();
            }
        }
               
        private Measurements grmMeasureCollection = null;
        public Measurements GrmMeasureCollection
        {
            get
            {
                if (grmMeasureCollection == null)
                    grmMeasureCollection = new Measurements();
                return grmMeasureCollection;
            }
            set
            {
                if (Equals(value, grmMeasureCollection)) return;
                grmMeasureCollection = value;
                NotifyPropertyChanged();
            }
        }

        private RecommendSizes grmMeasureSelectedCollec = null;
        public RecommendSizes GrmMeasureSelectedCollec
        {
            get
            {
                if (grmMeasureSelectedCollec == null)
                    grmMeasureSelectedCollec = new RecommendSizes();
                return grmMeasureSelectedCollec;
            }
            set
            {
                if (Equals(value, grmMeasureSelectedCollec)) return;
                grmMeasureSelectedCollec = value;
                NotifyPropertyChanged();
            }
        }

        public OrderDetails(OrderDetails previousOrder)
        {
            OrderID = previousOrder.OrderID;
            CustomerName = previousOrder.CustomerName;
            OrderGarment = new GarmentInfo( previousOrder.OrderGarment);
            OrderDescription = previousOrder.OrderDescription;
            OrderUnits = previousOrder.OrderUnits;
            orderScanned = previousOrder.orderScanned;
            OrderStage = previousOrder.OrderStage;
            OrderReceived = previousOrder.OrderReceived;
            OrderPath = previousOrder.OrderPath;
            BatchPath = previousOrder.BatchPath;
            OrderBaseSize = previousOrder.OrderBaseSize;
            CstmrMeasureCollection = DeepCopyMeasure(previousOrder.CstmrMeasureCollection);
            CstmrMeasureSelectedCollec = DeepCopyMeasure(previousOrder.CstmrMeasureSelectedCollec);
            GrmMeasureCollection = DeepCopyMeasure(previousOrder.GrmMeasureCollection);
            GrmMeasureSelectedCollec =  DeepCopyMeasure(previousOrder.GrmMeasureSelectedCollec);            
        }

        private static Measurements DeepCopyMeasure(Measurements collection)
        {
            Measurements newCollection = new Measurements();
            foreach (Measurement item in collection)
            {
                newCollection.Add(new Measurement(item));
            }
            return newCollection;
        }

        private static RecommendSizes DeepCopyMeasure(RecommendSizes collection)
        {
            RecommendSizes newCollection = new RecommendSizes();
            foreach (RecommendSize item in collection)
            {
                newCollection.Add(new RecommendSize(item));
            }
            return newCollection;
        }

        private static CustomersMeasurement DeepCopyMeasure(CustomersMeasurement collection)
        {
            CustomersMeasurement newCollection = new CustomersMeasurement();

            foreach (CustomerMeasurement item in collection)
            {               
                newCollection.Add(new CustomerMeasurement(item));
            }
            return newCollection;
        }

        public OrderDetails() { }

    }
    public class OrdersDetails : ObservableCollection<OrderDetails>
    {
        // Creating the collection in this way enables data binding from XAML.
    }
}
