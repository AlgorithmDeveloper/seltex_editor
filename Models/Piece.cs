﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Serialization;

namespace Models
{
    public class Piece : BaseModel
    {
        private int pieceID;
        [XmlIgnore]
        public int PieceID
        {
            get => pieceID; set
            {
                if (Equals(value, pieceID)) return;
                pieceID = value;
                NotifyPropertyChanged();
            }
        }

        private string pieceName;
        [XmlAttribute("Name")]
        public string PieceName
        {
            get => pieceName; set
            {
                if (Equals(value, pieceName)) return;
                pieceName = value;
                NotifyPropertyChanged();
            }
        }

        private string pieceDescription;
        [XmlIgnore]
        public string PieceDescription
        {
            get => pieceDescription; set
            {
                if (Equals(value, pieceDescription)) return;
                pieceDescription = value;
                NotifyPropertyChanged();
            }
        }

        [XmlElement("Parent_Piece")]  //  [XmlIgnore] 
        private Measurement parentMeasurement;
        public Measurement ParentMeasurement
        {
            get
            {
                if (parentMeasurement == null)
                    parentMeasurement = new Measurement();
                return parentMeasurement;
            }
            set
            {
                if (Equals(value, parentMeasurement)) return;
                parentMeasurement = value;
                NotifyPropertyChanged();
            }
        }


        private GradingRules gradingCollection = null;
        [XmlElement("Grading_Rules_List")]
        public GradingRules GradingCollection
        {
            get
            {
                if (gradingCollection == null)
                    gradingCollection = new GradingRules();
                return gradingCollection;
            }
            set
            {
                if (Equals(value, gradingCollection)) return;
                gradingCollection = value;
                NotifyPropertyChanged();
            }
        }
        
        public Piece(Piece previousPiece)
        {
            PieceID = previousPiece.pieceID;
            PieceName = previousPiece.PieceName;
            PieceDescription = String.IsNullOrEmpty(previousPiece.PieceDescription) ? string.Empty : previousPiece.PieceDescription;
            ParentMeasurement = new Measurement(previousPiece.ParentMeasurement);
            // GradingCollection = (GradingRules)new ObservableCollection<GradingRule>(previousPiece.GradingCollection.Select(x => x.Clone()).Cast<GradingRule>());
            // PieceCollection = (Pieces)new ObservableCollection<Piece>(previousMeasurement.PieceCollection.Select(x => x.Clone()));
            //   DeepCopy( previousMeasurement.PieceCollection);
            //for reference types
            // other.IdInfo = new IdInfo(IdInfo.IdNumber);
            //   Collar = new DogCollar(otherDog.Collar);
            GradingCollection = DeepCopy(previousPiece.GradingCollection);            
        }

        public Piece(int _pieceID, string _pieceName, GradingRules _gradingCollection = null, string _pieceDescription = "")
        {
            PieceID = _pieceID;
            PieceName = _pieceName;
            PieceDescription = String.IsNullOrEmpty(_pieceDescription) ? string.Empty : _pieceDescription;
            if (_gradingCollection != null)
                GradingCollection = DeepCopy(_gradingCollection);
            //  String.IsNullOrEmpty(...) -> true if the value parameter is null or an empty string(""); otherwise, false.
            //  evaluate to true or false.If condition is true, first_expression .If condition is false, second_expression   
        }        

        public Piece() { }

        private static GradingRules DeepCopy(GradingRules collection)
        {
            GradingRules newCollection = new GradingRules();

            foreach (GradingRule item in collection)
            {
                GradingRule newItem = new GradingRule(item);
                newCollection.Add(newItem);
            }
            return newCollection;
        }
    }
    public class Pieces : ObservableCollection<Piece>
    {        
        // Creating the collection in this way enables data binding from XAML.
    }

    //static class Extensions
    //{

    //public Piece DeepCopy()
    //{
    //    Piece other = (Piece)this.MemberwiseClone();
    //    other.PieceName = String.Copy(PieceName);
    //    return other;
    //}

    ////public Piece ShallowCopy()
    ////{
    ////    return (Piece)this.MemberwiseClone();
    ////}

    //    private static ObservableCollection<T> DeepCopy<T>(ObservableCollection<T> list) where T : ICloneable
    //    {
    //        ObservableCollection<T> newList = new ObservableCollection<T>();
    //        return new ObservableCollection<T>(list.Select(x => x.Clone()).Cast<T>());


    //        //foreach (T rec in list)
    //        //{
    //        //    newList.Add((T)rec.Clone());
    //        //}
    //        //return newList;
    //    }
    //    private static ObservableCollection<T> DeepCopy<T>(IEnumerable<T> list) where T : ICloneable
    //    {
    //        return new ObservableCollection<T>(list.Select(x => x.Clone()).Cast<T>());
    //    }
    //    //cars = new Cars();
    //    // originalCopy = new ObservableCollection<CarType>(from x in cars.Items select(CarType)x.Clone());
    //}
}
