﻿using System;
using System.Collections.ObjectModel;

namespace Models
{
    //size recommended from the style
    public class RecommendSize : Measurement
    {
        //private int rcmMeasureID;
        //public int RcmMeasureID
        //{
        //    get { return rcmMeasureID; } 
        //    set
        //    {
        //        if (Equals(value, rcmMeasureID)) return;
        //        rcmMeasureID = value;
        //        NotifyPropertyChanged();
        //    }
        //}
        //private string rcmMeasureName;
        //public string RcmMeasureName
        //{
        //    get { return rcmMeasureName; }
        //    set
        //    {
        //        if (Equals(value, rcmMeasureName)) return;
        //        rcmMeasureName = value;
        //        NotifyPropertyChanged();
        //    }
        //}
        private Double rcmMeasureValue;
        public Double RcmMeasureValue
        {
            get { return this.rcmMeasureValue; }
            set
            {
                if (Equals(value, rcmMeasureValue)) return;
                rcmMeasureValue = value;
                NotifyPropertyChanged();                
            }
        }

        private string delta;      
        public string Delta
        {
            get { return delta; }
            set
            {
                if (Equals(value, delta)) return;
                delta = value;
                NotifyPropertyChanged();
            }
        }

        
        public RecommendSize(RecommendSize previousRecommendSize)
        {
            MeasureID = previousRecommendSize.MeasureID;
            MeasureName = String.IsNullOrEmpty(previousRecommendSize.MeasureName) ? string.Empty : previousRecommendSize.MeasureName;
            PieceCollection = new Pieces();
            MeasureDescription = String.IsNullOrEmpty(previousRecommendSize.MeasureDescription) ? string.Empty : previousRecommendSize.MeasureDescription;
            RcmMeasureValue = previousRecommendSize.RcmMeasureValue;
            Delta = String.IsNullOrEmpty(previousRecommendSize.Delta) ? string.Empty : previousRecommendSize.Delta;
        }        

        public RecommendSize(Measurement previousMeasurement) : base (previousMeasurement)
        {  
            RcmMeasureValue = 0.0;
            Delta = string.Empty;
        }

        public RecommendSize(int _recMeasureID, string _recMeasureName,  double _recMeasureValue, string _delta): base(_recMeasureID, _recMeasureName )
        {
            RcmMeasureValue = _recMeasureValue;            
            Delta = String.IsNullOrEmpty(_recMeasureName) ? string.Empty : _delta;
        }
        public RecommendSize() { }
    }
    public class RecommendSizes : ObservableCollection<RecommendSize>
    {
        // Creating the collection in this way enables data binding from XAML.
    }

}
