﻿using System.Collections.ObjectModel;

namespace Models
{
    public class RuleLibrary : BaseModel
    {
        private Piece myPiece;
        public Piece MyPiece
        {
            get { return myPiece; }
            set
            {
                if (Equals(value, myPiece)) return;
                myPiece = value;
                NotifyPropertyChanged();
            }
        }

        private Vertices vertexCollection = null; // vertexCollection
        public Vertices VertexCollection
        {
            get
            {
                if (vertexCollection == null)
                    vertexCollection = new Vertices();
                return vertexCollection;
            }
            set
            {
                if (Equals(value, vertexCollection)) return;
                vertexCollection = value;
                NotifyPropertyChanged();
            }
        }

        public RuleLibrary(RuleLibrary previousRuleLibrary)
        {
            MyPiece = new Piece(previousRuleLibrary.MyPiece);
            VertexCollection = DeepCopy(previousRuleLibrary.VertexCollection);
        }

        public RuleLibrary(Piece _myPiece, Vertices _pointsCollection)
        {
            MyPiece = new Piece(_myPiece.PieceID, _myPiece.PieceName);
            VertexCollection = _pointsCollection;
        }      
       
        public RuleLibrary() { }

        private static Vertices DeepCopy(Vertices collection)
        {
            Vertices newCollection = new Vertices();

            foreach (Vertex item in collection)
            {
                Vertex newItem = new Vertex(item);
                newCollection.Add(newItem);
            }
            return newCollection;
        }
    }
    public class RulesLibraries : ObservableCollection<RuleLibrary>
    {
        // Creating the collection in this way enables data binding from XAML.
    }
}
