﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Models
{
    public class Vertex : BaseModel
    {
        private int vertexID;
        [XmlIgnore]
        public int VertexID
        {
            get { return  vertexID; }            
            set
            {
                if (Equals(value,  vertexID)) return;
                 vertexID = value;
                NotifyPropertyChanged();
            }
        }

        private string  vertexName;
        [XmlElement("Name")]
        public string VertexName
        {
            get { return  vertexName; }            
            set
            {
                if (Equals(value,  vertexName)) return;
                 vertexName = value;
                NotifyPropertyChanged();
            }
        }

        private string  vertexDescription;
        [XmlIgnore]
        public string VertexDescription
        {
            get { return  vertexDescription; }
            set
            {
                if (Equals(value,  vertexDescription)) return;
                 vertexDescription = value;
                NotifyPropertyChanged();
            }
        }

        public Vertex(Vertex previousVertex)
        {
            VertexID = previousVertex.VertexID;
            VertexName = previousVertex.VertexName;
            VertexDescription = String.IsNullOrEmpty(previousVertex.VertexDescription) ? string.Empty : previousVertex.VertexDescription;
        }
        public Vertex(int _vertexID, string _vertexName, string _vertexDescription = "")
        {
            VertexID = _vertexID;
            VertexName = _vertexName;
            //  String.IsNullOrEmpty(...) -> true if the value parameter is null or an empty string(""); otherwise, false.
            //  evaluate to true or false.If condition is true, first_expression .If condition is false, second_expression           
            VertexDescription = String.IsNullOrEmpty(_vertexDescription) ? string.Empty : _vertexDescription;            
        }

        public Vertex() { }
    }
    public class Vertices : ObservableCollection<Vertex>
    {
        // Creating the collection in this way enables data binding from XAML.
    }

    public class ProductComparer : IEqualityComparer<Vertex>
    {

        public bool Equals(Vertex x, Vertex y)
        {
            //Check whether the objects are the same object. 
            if (Object.ReferenceEquals(x, y)) return true;

            //Check whether the products' properties are equal. 
            return x != null && y != null && x.VertexName.Equals(y.VertexName) && x.VertexName.Equals(y.VertexName);
        }

        public int GetHashCode(Vertex obj)
        {
            //Get hash code for the Name field if it is not null. 
            int hashProductName = obj.VertexName == null ? 0 : obj.VertexName.GetHashCode();

            //Get hash code for the Code field. 
            int hashProductCode = obj.VertexName.GetHashCode();

            //Calculate the hash code for the product. 
            return hashProductName ^ hashProductCode;
        }
    }
}
