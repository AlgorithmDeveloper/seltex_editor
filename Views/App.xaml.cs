﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Linq;

namespace Views
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        App()
        {
            // To create a string that specifies the path to a subfolder under your 
            // top-level folder, add a name for the subfolder to folderName.
            string pathDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "SelteXEditor");
            if (!Directory.Exists(pathDirectory))
            {
                Directory.CreateDirectory(pathDirectory);
            }


            string fileNameWithExt = Views.Properties.Settings.Default.GradeRulesXmlFileName;
            // Use Combine again to add the file name to the path.
            string pathfile = Path.Combine(pathDirectory, fileNameWithExt);

            if (!File.Exists(pathfile))
            {
                File.Create(pathfile).Close();
            }
            if (File.Exists(pathfile) && Views.Properties.Settings.Default.GradeRulesXmlPath == "")
            {
                Views.Properties.Settings.Default.GradeRulesXmlPath = pathfile;
            }

            fileNameWithExt = Views.Properties.Settings.Default.StylesXmlFileName;
            pathfile = Path.Combine(pathDirectory, fileNameWithExt);

            if (!File.Exists(pathfile))
            {
                File.Create(pathfile).Close();
            }

            if (File.Exists(pathfile) && Views.Properties.Settings.Default.StylesXmlPath == "")
            {
                Views.Properties.Settings.Default.StylesXmlPath = pathfile;
            }

            string targetDirectory = Environment.ExpandEnvironmentVariables("%ProgramW6432%");

            
            if (File.Exists(Path.Combine(targetDirectory, "Optitex 15", "App", "PDS15.exe")))
            {     
                Views.Properties.Settings.Default.OptitexAppPath = (Path.Combine(targetDirectory, "Optitex 15", "App", "PDS15.exe"));               
            }
            
        }        
        
    }
}
