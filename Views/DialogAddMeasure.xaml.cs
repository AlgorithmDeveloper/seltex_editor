﻿using Models;
using System.Windows;

namespace Views
{
    /// <summary>
    /// Interaction logic for DialogAddMeasure.xaml
    /// </summary>
    public partial class DialogAddMeasure : Window
    {
        private Measurements localAllPiecas = new Measurements();
        public Measurements LocalAllPiecas
        {
            get
            {
                return localAllPiecas;
            }
            set
            {
                localAllPiecas = value;
            }
        }

        public DialogAddMeasure()
        {
            InitializeComponent();
        }
        public DialogAddMeasure(Measurements _ruleLibrary)
        {
            InitializeComponent();
            if (_ruleLibrary.Count > 0)
            {
                LocalAllPiecas = _ruleLibrary;
                measureComboBox.ItemsSource = LocalAllPiecas;
            }
        }
        public Measurement MeasureSelected()
        {
            if (measureComboBox.SelectedIndex != -1)
            {
                Measurement _chosenMeasure = measureComboBox.SelectedValue as Measurement;
                return _chosenMeasure;
            }
            else
                return null;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box accepted
            this.DialogResult = true;

        }
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            this.DialogResult = false;
        }
    }
}
