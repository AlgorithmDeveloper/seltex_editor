﻿using System.Windows;
using Models;
using System.Collections.ObjectModel;

namespace Views
{
    /// <summary>
    /// Interaction logic for DialogAddPiece.xaml
    /// </summary>
    public partial class DialogAddPiece : Window
    {
        private RulesLibraries piecaCollection = new RulesLibraries();
        internal RulesLibraries PiecaCollection
        {
            get
            {
                return piecaCollection;
            }
            set
            {
                piecaCollection = value;
            }
        }
        public DialogAddPiece()
        {
            InitializeComponent();
        }
        public DialogAddPiece(RulesLibraries _ruleLibrary)
        {
            InitializeComponent();
            if (_ruleLibrary.Count > 0)
            {
                PiecaCollection = _ruleLibrary;
                cmbBxPiece.ItemsSource = PiecaCollection;
            }
        }

        public Piece PieceSelected()
        {
            if (cmbBxPiece.SelectedIndex != -1)
            {
                Piece chosenPiece = cmbBxPiece.SelectedValue as Piece;
                return chosenPiece;
            }
            else
                return null;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box accepted
            this.DialogResult = true;
            //  RuleLibrary slct1 = pieceComboBox.SelectionBoxItem as RuleLibrary;
            //  RuleLibrary slct2 = pieceComboBox.SelectedItem as RuleLibrary;
        }
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            this.DialogResult = false;
        }
    }
}
