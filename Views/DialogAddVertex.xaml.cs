﻿using Models;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace Views
{
    /// <summary>
    /// Interaction logic for DialogAddVertex.xaml
    /// </summary>
    public partial class DialogAddVertex : Window
    {
        private Vertices vertexCollection = null;
        public Vertices VertexCollection
        {
            get => vertexCollection; set => vertexCollection = value;
        }

        private Vertices vertexCollectionPF = null;
        public Vertices VertexCollectionPF
        {
            get => vertexCollectionPF; set => vertexCollectionPF = value;
        }

        private Vertices vertexCollectionPL = null;
        public Vertices VertexCollectionPL
        {
            get => vertexCollectionPL; set => vertexCollectionPL = value;
        }

        private Vertices vertexCollectionBackup = null;
        public Vertices VertexCollectionBackup
        {
            get => vertexCollectionBackup; set => vertexCollectionBackup = value;
        }

        public Vertex OldVertexPF { get; set; }

        public Vertex OldVertexPL { get; set; }

        public DialogAddVertex()
        {
            InitializeComponent();
        }
        public DialogAddVertex(Vertices _vertexCollectionfilter, Vertices _vertexCollection)
        {
            InitializeComponent();
            if (_vertexCollection != null)
            {
                VertexCollection = new Vertices();
                VertexCollectionPF = new Vertices();
                VertexCollectionPL = new Vertices();
                VertexCollectionBackup = new Vertices();
                foreach (var item in _vertexCollectionfilter)
                {
                    Vertex newItem = new Vertex(item);
                    VertexCollection.Add(newItem);
                }
                foreach (var item in _vertexCollection)
                {
                    Vertex newItem = new Vertex(item);
                    VertexCollectionPF.Add(newItem);
                    VertexCollectionPL.Add(newItem);
                    VertexCollectionBackup.Add(newItem);
                }

                cbVertex.ItemsSource = VertexCollection;
                cbVertexPF.ItemsSource = VertexCollectionBackup;// VertexCollectionPF;
                cbVertexPL.ItemsSource = VertexCollectionBackup;//VertexCollectionPL;
            }
        }
        public Vertex SelectedVertex()// The SelectedVertex
        {
            if (cbVertex.SelectedIndex != -1)
            {
                return cbVertex.SelectedItem as Vertex;
            }
            else
                return null;
        }
        public Vertex SelectedVertexPF()// The SelectedVertex
        {
            if (cbVertexPF.SelectedIndex != -1)
            {
                return cbVertexPF.SelectedItem as Vertex;
            }
            else
                return null;
        }
        public Vertex SelectedVertexPL()// The SelectedVertex
        {
            if (cbVertexPL.SelectedIndex != -1)
            {
                return cbVertexPL.SelectedItem as Vertex;
            }
            else
                return null;
        }
        void okButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box accepted
            this.DialogResult = true;
        }
        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            this.DialogResult = false;
        }

        private void cbVertex_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //Vertex moving = new Vertex(SelectedVertex());
            //if (cbVertexPF.SelectedIndex == -1)
            //{
            //    if (VertexCollectionPF.Any(vertex => vertex.VertexName == moving.VertexName))
            //    {
            //        VertexCollectionPF.Remove(VertexCollectionPF.FirstOrDefault(vertex => vertex.VertexName == moving.VertexName));
            //    }
            //}
            //else //(cbVertexPL.SelectedIndex != -1)
            //{
            //    Vertex movingPF = new Vertex(SelectedVertexPF());
            //    if (movingPF.VertexName == moving.VertexName)
            //    {
            //        VertexCollectionPF = new Vertices();
            //        foreach (var item in VertexCollectionBackup)
            //        {
            //            VertexCollectionPF.Add(new Vertex(item));
            //        }
            //    }
            //    else
            //    {
            //        var dif = VertexCollectionPF.Where(p => !VertexCollectionBackup.Any(l => p.VertexName == l.VertexName));
            //        foreach (var item in dif)
            //        {
            //            VertexCollectionPF.Add(new Vertex(item));
            //        }
            //    }
            //    if (VertexCollectionPF.Any(vertex => vertex.VertexName == moving.VertexName))
            //    {
            //        VertexCollectionPF.Remove(VertexCollectionPF.FirstOrDefault(vertex => vertex.VertexName == moving.VertexName));
            //    }
            //    if (cbVertexPL.SelectedIndex != -1)
            //    {
            //        Vertex movingPL = new Vertex(SelectedVertexPL());
            //        if (VertexCollectionPF.Any(vertex => vertex.VertexName == movingPL.VertexName))
            //        {
            //            VertexCollectionPF.Remove(VertexCollectionPF.FirstOrDefault(vertex => vertex.VertexName == movingPL.VertexName));
            //        }
            //    }
            //}
            ////PL REG
            //if (cbVertexPL.SelectedIndex == -1)
            //{
            //    if (VertexCollectionPL.Any(vertex => vertex.VertexName == moving.VertexName))
            //    {
            //        VertexCollectionPL.Remove(VertexCollectionPL.FirstOrDefault(vertex => vertex.VertexName == moving.VertexName));
            //    }
            //}
            //else
            //{
            //    Vertex movingPL = new Vertex(SelectedVertexPF());
            //    if (movingPL.VertexName == moving.VertexName)
            //    {
            //        VertexCollectionPL = new Vertices();
            //        foreach (var item in VertexCollectionBackup)
            //        {
            //            VertexCollectionPL.Add(new Vertex(item));
            //        }
            //    }
            //    else
            //    {
            //        var dif = VertexCollectionPL.Where(p => !VertexCollectionBackup.Any(l => p.VertexName == l.VertexName));
            //        foreach (var item in dif)
            //        {
            //            VertexCollectionPL.Add(new Vertex(item));
            //        }
            //    }
            //    if (VertexCollectionPL.Any(vertex => vertex.VertexName == moving.VertexName))
            //    {
            //        VertexCollectionPL.Remove(VertexCollectionPL.FirstOrDefault(vertex => vertex.VertexName == moving.VertexName));
            //    }
            //    if (cbVertexPF.SelectedIndex != -1)
            //    {
            //        Vertex movingPF = new Vertex(SelectedVertexPF());
            //        if (VertexCollectionPL.Any(vertex => vertex.VertexName == movingPF.VertexName))
            //        {
            //            VertexCollectionPL.Remove(VertexCollectionPL.FirstOrDefault(vertex => vertex.VertexName == movingPF.VertexName));
            //        }
            //    }
           // }
            





            //else
            //{
            //    VertexCollectionPL.Add(new Vertex(moving));
            //    //Vertex newItem = new Vertex(moving);
            //    //VertexCollectionPL.Add(newItem);
            //}

            //else
            //{
            //    VertexCollectionPF.Add(new Vertex(moving));
            //    //Vertex newItem = new Vertex(moving);
            //    //VertexCollectionPF.Add(newItem);
            //}




            //VertexCollectionPF = new Vertices();
            //VertexCollectionPL = new Vertices();

            //foreach (var item in VertexCollection)
            //{
            //    if (item.VertexName != moving.VertexName)
            //    {
            //        Vertex newItem = new Vertex(item);
            //        VertexCollectionPF.Add(newItem);
            //        VertexCollectionPL.Add(newItem);
            //    }
            //}
            //cbVertexPF.ItemsSource = VertexCollectionPF;
            //cbVertexPL.ItemsSource = VertexCollectionPL;
        }

        private void cbVertexPF_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //Vertex moving = new Vertex(SelectedVertex());
            //Vertex movingPF = new Vertex(SelectedVertexPF());

            //bool equalVertex = false;
            //if (cbVertexPL.SelectedIndex != -1)
            //{
            //    Vertex movingPL = new Vertex(SelectedVertexPL());
            //    if (movingPL.VertexName == movingPF.VertexName)
            //    {
            //        equalVertex = true;
            //    }
            //}
            //if (cbVertexPL.SelectedIndex == -1 || equalVertex)
            //{
            //    VertexCollectionPL = new Vertices();
            //    foreach (var item in VertexCollection)
            //    {
            //        if (item.VertexName != movingPF.VertexName && item.VertexName != moving.VertexName)
            //        {
            //            Vertex newItem = new Vertex(item);
            //            VertexCollectionPL.Add(newItem);
            //        }
            //    }
            //    cbVertexPL.ItemsSource = VertexCollectionPL;
            //}
            //else
            //{
            //    foreach (var item in VertexCollectionPF)
            //    {
            //        // VertexCollectionPL.Any(vertex => vertex.VertexName == item.VertexName);
            //        if (!VertexCollectionPL.Any(vertex => vertex.VertexName == item.VertexName))
            //        {
            //            Vertex newItem = new Vertex(item);
            //            VertexCollectionPL.Add(newItem);
            //        }
            //    }
            //    // VertexCollectionPL.FirstOrDefault(vertex => vertex.VertexName == movingPF.VertexName);
            //    VertexCollectionPL.Remove(VertexCollectionPL.FirstOrDefault(vertex => vertex.VertexName == movingPF.VertexName));
            //    VertexCollectionPL.OrderBy(vertex => vertex.VertexName);
            //}
        }

        private void cbVertexPL_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //Vertex moving = new Vertex(SelectedVertex());
            //Vertex movingPL = new Vertex(SelectedVertexPL());

            //bool equalVertex = false;
            //if (cbVertexPF.SelectedIndex != -1)
            //{
            //    Vertex movingPF = new Vertex(SelectedVertexPF());
            //    if (movingPL.VertexName == movingPF.VertexName)
            //    {
            //        equalVertex = true;
            //    }
            //}

            //if (cbVertexPF.SelectedIndex == -1 || equalVertex)
            //{
            //    VertexCollectionPF = new Vertices();
            //    foreach (var item in VertexCollection)
            //    {
            //        if (item.VertexName != movingPL.VertexName && item.VertexName != moving.VertexName)
            //        {
            //            Vertex newItem = new Vertex(item);
            //            VertexCollectionPF.Add(newItem);
            //        }
            //    }
            //    cbVertexPF.ItemsSource = VertexCollectionPF;
            //}
            //else
            //{
            //    foreach (var item in VertexCollectionPL)
            //    {
            //        if (!VertexCollectionPF.Any(vertex => vertex.VertexName == item.VertexName))
            //        {
            //            Vertex newItem = new Vertex(item);
            //            VertexCollectionPF.Add(newItem);
            //        }
            //    }
            //    VertexCollectionPF.Remove(VertexCollectionPF.FirstOrDefault(vertex => vertex.VertexName == movingPL.VertexName));
            //    VertexCollectionPF.OrderBy(vertex => vertex.VertexName);
            //}
        }
    }
}
//ComboBox cmb = (ComboBox)sender;
//int selectedIndex = cmb.SelectedIndex;
//int selectedValue = (int)cmb.SelectedValue;

//ComboboxItem selectedCar = (ComboboxItem)cmb.SelectedItem;