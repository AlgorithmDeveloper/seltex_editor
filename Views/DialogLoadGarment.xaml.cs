﻿using Microsoft.Win32;
using Models;
using System;
using System.IO;
using System.Windows;
using Views.Utilities;

namespace Views
{
    /// <summary>
    /// Interaction logic for DialogLoadGarment.xaml
    /// </summary>
    public partial class DialogLoadGarment : Window
    {
        private GarmentInfo garment = null;
        public GarmentInfo Garment
        {
            get => garment; set => garment = value;
        }

        private Measurements measureLoad = null;
        public Measurements MeasureLoad
        {
            get => measureLoad; set => measureLoad = value;
        }

        private RulesLibraries rulesLoad = null;
        public RulesLibraries RulesLoad
        {
            get => rulesLoad; set => rulesLoad = value;
        }

        public string GarmentDescription { get; set; }        

        public DialogLoadGarment()
        {
            InitializeComponent();
        }

        public DialogLoadGarment(ref Measurements _measureCollection, ref RulesLibraries _ruleCollection)
        {
            InitializeComponent();
            if (MeasureLoad == null)
                MeasureLoad = new Measurements();
            MeasureLoad = DeepCopyMeasures(_measureCollection);
            if (RulesLoad == null)
                RulesLoad = new RulesLibraries();
            RulesLoad = DeepCopyRules(_ruleCollection);
        }

        private void BtnOpenPDS_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tbPDS_File.Clear();
                OpenFileDialog openFileDialog = new OpenFileDialog()
                {
                    // Allow multiple file selection.              
                    Multiselect = false,
                    DefaultExt = ".PDS",
                    Filter = "Style file (*.PDS)|*.PDS",
                    Title = "Select One Style",
                    CheckFileExists = true,
                    CheckPathExists = true
                };
                openFileDialog.ShowDialog();

                if (openFileDialog.FileName.Length > 0)
                {
                    tbPDS_File.Text = Path.GetFileName(openFileDialog.FileName);
                    Garment = new GarmentInfo()
                    {
                        GarmentName = Path.GetFileNameWithoutExtension(openFileDialog.FileName),                        
                        GarmentPath = openFileDialog.FileName
                    };
                    GarmentDescription = rtbStyleDesc.Text;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void BtnOpenDMC_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExcelParserClass mAppExcel = new ExcelParserClass();

                mAppExcel.BrowseExcelDialog();
                if (mAppExcel.openFileDialog.FileName.Length > 0)
                {
                    mAppExcel.OpenExcelFile(mAppExcel.openFileDialog.FileName, 1);
                    tbDMC_File.Text = mAppExcel.openFileDialog.SafeFileName;
                    Garment.DMCPath = mAppExcel.openFileDialog.FileName;
                }
                if (mAppExcel.excelSheet != null && mAppExcel.excelRange != null)
                {
                    MeasureLoad = new Measurements();
                    mAppExcel.ImportMeasure(ref measureLoad, mAppExcel.excelRange);
                    Garment.MeasureCollection = DeepCopyMeasures(measureLoad);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void BtnOpenRules_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExcelParserClass mAppExcel = new ExcelParserClass();

                mAppExcel.BrowseExcelDialog();
                if (mAppExcel.openFileDialog.FileName.Length > 0)
                {
                    mAppExcel.OpenExcelFile(mAppExcel.openFileDialog.FileName, 1);
                    tbRulesFiles.Text = mAppExcel.openFileDialog.SafeFileName;
                    Garment.RulePath = mAppExcel.openFileDialog.FileName;
                }
                if (mAppExcel.excelSheet != null && mAppExcel.excelRange != null)
                {
                    RulesLoad = new RulesLibraries();
                    mAppExcel.ImportRules<RulesLibraries>(ref rulesLoad, mAppExcel.excelRange);
                    Garment.RuleCollection = DeepCopyRules(rulesLoad);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        public GarmentInfo GetNewGarment()
        {
            if (Garment != null)
            {
                return Garment;
            }
            else
                return null;
        }

        public Measurements GetNewMeasurements()
        {
            if (MeasureLoad != null)
            {
                return MeasureLoad;
            }
            else
                return null;
        }

        public RulesLibraries GetRulesLibraries()
        {
            if (RulesLoad != null)
            {
                return RulesLoad;
            }
            else
                return null;
        }

        public string GetDescription()
        {
            return GarmentDescription;
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            //string directory = Path.GetDirectoryName(Garment.GarmentPath);  //
            //string fileName = Path.GetFileNameWithoutExtension(Garment.GarmentPath);
            //string fileNameWithExt = fileName + ".xml";
            //Garment.GradeRuleXmlPath = Path.Combine(directory, fileNameWithExt);



            GarmentDescription = rtbStyleDesc.Text;

            // Dialog box add
            this.DialogResult = true;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            this.DialogResult = false;
        }


        protected static Measurements DeepCopyMeasures(Measurements collection)
        {
            Measurements newCollection = new Measurements();

            foreach (Measurement item in collection)
            {
                Measurement newItem = new Measurement(item);
                newCollection.Add(newItem);
            }
            return newCollection;
        }

        protected static RulesLibraries DeepCopyRules(RulesLibraries collection)
        {
            RulesLibraries newCollection = new RulesLibraries();

            foreach (RuleLibrary item in collection)
            {
                RuleLibrary newItem = new RuleLibrary(item);
                newCollection.Add(newItem);
            }
            return newCollection;
        }
    }

}

