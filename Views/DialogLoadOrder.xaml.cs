﻿using Microsoft.Win32;
using Models;
using System;
using System.IO;
using System.Windows;

namespace Views
{
    /// <summary>
    /// Interaction logic for DialogLoadOrder.xaml
    /// </summary>
    /// 
    public partial class DialogLoadOrder : Window
    {
        
        private Garments gradeRulesLoad = null;
        public Garments GradeRulesLoad
        {
            get => gradeRulesLoad; set => gradeRulesLoad = value;
        }

        private OrderDetails orderDetailsLoad = null;
        public OrderDetails OrderDetailsLoad
        {
            get => orderDetailsLoad; set => orderDetailsLoad = value;
        }


        public DialogLoadOrder()
        {
            InitializeComponent();
        }

        public DialogLoadOrder(Garments _gradingRulesTree)
        {
            InitializeComponent();
            GradeRulesLoad = new Garments();
            foreach (Garment item in _gradingRulesTree)
            {                
                GradeRulesLoad.Add(new Garment(item));
            }            
            cmbBxStyle.ItemsSource = GradeRulesLoad;
        }
       

        private void BtnOpenORD_File_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog()
                {
                    // Allow multiple file selection.              
                    Multiselect = false,
                    DefaultExt = ".ord",
                    Filter = "Order files (*.ord)|*.ord",
                    // Set the text for the title bar.
                    Title = "Select Order"
                };
                openFileDialog.ShowDialog();
                if (openFileDialog.FileName.Length > 0)
                {
                   
                    Random rnd = new Random();
                    string line = string.Empty, subLine = string.Empty, value = string.Empty;
                    int equalSign = 0, TabSign = 0, count = 0;

                    tbORD_File.Text = Path.GetFileName(openFileDialog.FileName);
                    OrderDetailsLoad = new OrderDetails()
                    {
                        OrderPath = openFileDialog.FileName,
                        OrderID = rnd.Next(100, 301)
                    };                   
                    StreamReader reader = File.OpenText(openFileDialog.FileName);                    
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Substring(0, "MEASURE".Length) == "MEASURE")
                        {
                            count++;
                            equalSign = line.IndexOf('=');
                            TabSign = line.IndexOf('\t');
                            subLine = line.Substring(8, equalSign - 8);
                            value = line.Substring(equalSign + 1, TabSign - (equalSign + 1));

                            OrderDetailsLoad.CstmrMeasureCollection.Add(new CustomerMeasurement()
                            {
                                CstmrMeasureID = count,
                                CstmrMeasureName = subLine,
                                CstmrMeasureValue = value                                
                            });
                        }
                        else if (line.Contains("CUSTOMER"))
                        {
                            equalSign = line.IndexOf('=');
                            TabSign = line.IndexOf('\t');
                            OrderDetailsLoad.CustomerName = line.Substring(equalSign + 1, TabSign - (equalSign + 1));
                        }
                        else if (line.Substring(0, "GARMENT".Length) == "GARMENT")
                        {
                            equalSign = line.IndexOf('=');
                            TabSign = line.IndexOf('\t');
                            OrderDetailsLoad.OrderDescription = line.Substring(equalSign + 1, TabSign - (equalSign + 1));
                        }
                        else if(line.Contains("OPTION UNITS"))
                        {
                            equalSign = line.IndexOf('=');
                            TabSign = line.IndexOf('\t');
                            OrderDetailsLoad.OrderUnits = line.Substring(equalSign + 1, TabSign - (equalSign + 1)).ToUpper();
                        }
                        else if (line.Contains("\"#DATE"))
                        {
                            equalSign = line.IndexOf('=');
                            TabSign = line.IndexOf('\t');
                            OrderDetailsLoad.OrderScanned = Convert.ToDateTime(line.Substring(equalSign + 1, TabSign - (equalSign + 2)));
                            OrderDetailsLoad.OrderReceived = DateTime.Now;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        public CustomersMeasurement GetOrderMeasure()
        {
            if (OrderDetailsLoad.CstmrMeasureCollection != null)
            {
                return OrderDetailsLoad.CstmrMeasureCollection;
            }
            else
                return null;
        }

        public OrderDetails GetNewOrder()
        {
            if (OrderDetailsLoad != null)
            {
                return OrderDetailsLoad;
            }
            else
                return null;
        }

        public Garment GarmentSelected()
        {
            if (cmbBxStyle.SelectedIndex != -1)
            {
                Garment chosenGarment = cmbBxStyle.SelectedItem as Garment;
                return chosenGarment;
            }
            else
                return null;
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box add
            this.DialogResult = true;
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            this.DialogResult = false;
        }

        //public Measurements GetNewMeasurements()
        //{
        //    //if (OrderDetailsLoad != null)
        //    //{
        //    //    return OrderDetailsLoad;
        //    //}
        //    //else
        //        return null;
        //}

        //public Garments GetRulesLibraries()
        //{
        //    //if (OrderDetailsLoad != null)
        //    //{
        //    //    return GradeRulesLoad;
        //    //}
        //    //else
        //       return null;
        //}



        //    private void BtnOpenRules_Click(object sender, RoutedEventArgs e)
        //    {
        //        //try
        //        //{
        //        //    OpenFileDialog openFileDialog = new OpenFileDialog()
        //        //    {
        //        //        // Allow multiple file selection.              
        //        //        Multiselect = false,
        //        //        DefaultExt = ".xml",
        //        //        Filter = "Rule files (*.xml)|*.xml",
        //        //        // Set the text for the title bar.
        //        //        Title = "Select Rule File "
        //        //    };
        //        //    openFileDialog.ShowDialog();
        //        //    if (openFileDialog.FileName.Length > 0)
        //        //    {
        //        //        tbRulesFile.Text = openFileDialog.SafeFileName;
        //        //        GradeRulesLoad = new Garments();
        //        //        MyXmlSerializer.DeserializeObject(ref gradeRulesLoad, openFileDialog.FileName);
        //        //    }
        //        //}
        //        //catch (Exception ex)
        //        //{
        //        //    MessageBox.Show(ex.Message, ex.Source);
        //        //}
        //    }
        //}
    }
}
