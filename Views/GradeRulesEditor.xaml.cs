﻿using Views.ViewModels;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Views
{
    /// <summary>
    /// Interaction logic for GradeRulesEditor.xaml
    /// </summary>
    public partial class GradeRulesEditor : UserControl
    {
        
       VMgradeRulesEditor viewModelGrade = null;       


        public GradeRulesEditor()
        {
            InitializeComponent();
            viewModelGrade = (VMgradeRulesEditor)FindResource("VM_GRE");
        }
        

        private void Ellipse_MouseDown1(object sender, MouseButtonEventArgs e)
        {
            viewModelGrade.AddMeasureCommand.Execute(sender);
            //try
            //{
            //    DialogAddMeasure dlgAddMeasure = new DialogAddMeasure(MeasureCollection);
            //    bool? result = dlgAddMeasure.ShowDialog();

            //    if (result.Value)
            //    {
            //        Measurement child = new Measurement(dlgAddMeasure.MeasureSelected());
            //        if (child != null)
            //        {
            //            var elip = sender as Ellipse;
            //            ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(elip));
            //            GarmentInfo parent = (GarmentInfo)presenter.Content;
            //            parent.MeasureCollection.Add(child);
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show("Not chosen measurement");
            //    }
            //    dlgAddMeasure.Close();
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}
        }

        private void Ellipse_MouseDown2(object sender, MouseButtonEventArgs e)
        {
            viewModelGrade.AddPieceCommand.Execute(sender);
            //try
            //{
            //    Piece child = null;

            //    DialogAddPiece dlgAddPiece = new DialogAddPiece(RuleCollection);
            //    bool? result = dlgAddPiece.ShowDialog();
            //    if (result.Value)
            //    {
            //        child = new Piece(dlgAddPiece.PieceSelected());
            //    }
            //    dlgAddPiece.Close();
            //    if (child != null)
            //    {
            //        var elip = sender as Ellipse;
            //        ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(elip));
            //        Measurement parent = (Measurement)presenter.Content;
            //        parent.PieceCollection.Add(child);
            //    }
            //    else
            //    {
            //        MessageBox.Show("Not chosen piece");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}
        }

        private void Rectangle_MouseDown2(object sender, MouseButtonEventArgs e)
        {
            viewModelGrade.DeleteMeasureCommand.Execute(sender);
            //try
            //{
            //    var rect = sender as Rectangle;
            //    ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(rect));
            //    Measurement parent = (Measurement)presenter.Content;
            //    //bool emailExists = GarmentTree.Any(x => x.MeasureCollection.Any(y => y.MeasureName == parent.MeasureName));
            //    //       var find2 = (Measurements)GarmentTree.SelectMany(petOwner => petOwner.MeasureCollection.Where(y => y.MeasureName == parent.MeasureName));

            //    foreach (var item in GarmentTree)
            //    {
            //        foreach (var y in item.MeasureCollection)
            //        {
            //            if (y.MeasureName == parent.MeasureName)
            //            {
            //                item.MeasureCollection.Remove(y);
            //                return;
            //            }
            //        }
            //    }
            //}

            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}
        }

        private void Ellipse_MouseDown3(object sender, MouseButtonEventArgs e)
        {
            viewModelGrade.AddVertexCommand.Execute(sender);
            //try
            //{
            //    GradingRule child = null, DeepCopyChild = null;
            //    Vertex vertex = null, vertexPF = null, vertexPL = null;
            //    DialogAddVertex dlgAddVertex = null;
            //    var elip = sender as Ellipse;
            //    ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(elip));
            //    Piece parent = (Piece)presenter.Content;

            //    RuleLibrary select = RuleCollection.FirstOrDefault(currentPiece => currentPiece.MyPiece.PieceName == parent.PieceName);
            //    if (select != null)
            //    {
            //        dlgAddVertex = new DialogAddVertex(select);
            //        bool? result = dlgAddVertex.ShowDialog();
            //        if (result.Value)
            //        {
            //            vertex = new Vertex(dlgAddVertex.SelectedVertex());
            //            vertexPF = dlgAddVertex.SelectedVertexPF();
            //            vertexPL = dlgAddVertex.SelectedVertexPL();
            //        }
            //    }
            //    dlgAddVertex.Close();
            //    if (vertex != null)
            //    {
            //        child = new GradingRule()
            //        {
            //            VertexName = vertex.VertexName
            //        };
            //        if (vertexPF != null)
            //        {
            //            child.VertexFirst = new Vertex(vertexPF);
            //        }
            //        else
            //        {
            //            child.VertexFirst = new Vertex();
            //        }
            //        if (vertexPL != null)
            //        {
            //            child.VertexLast = new Vertex(vertexPL);
            //        }
            //        else
            //        {
            //            child.VertexLast = new Vertex();
            //        }
            //        DeepCopyChild = new GradingRule(child);

            //        parent.GradingCollection.Add(DeepCopyChild);
            //    }
            //    else
            //    {
            //        MessageBox.Show("Not chosen point");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}
        }

        private void Rectangle_MouseDown3(object sender, MouseButtonEventArgs e)
        {
            viewModelGrade.DeletePieceCommand.Execute(sender);
            //try
            //{
            //    var rect = sender as Rectangle;
            //    ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(rect));
            //    Piece parent = (Piece)presenter.Content;


            //    foreach (var item in GarmentTree)
            //    {
            //        foreach (var y in item.MeasureCollection)
            //        {
            //            foreach (var x in y.PieceCollection)
            //            {
            //                if (x.PieceName == parent.PieceName)
            //                {
            //                    y.PieceCollection.Remove(x);
            //                    return;
            //                }
            //            }

            //        }
            //    }
            //}

            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}
        }

        private void Rectangle_MouseDown4(object sender, MouseButtonEventArgs e)
        {
            viewModelGrade.DeleteVertexCommand.Execute(sender);
            //try
            //{
            //    var rect = sender as Rectangle;
            //    ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(rect));
            //    Vertex parent = (Vertex)presenter.Content;

            //    foreach (var item in GarmentTree)
            //    {
            //        foreach (var y in item.MeasureCollection)
            //        {
            //            foreach (var x in y.PieceCollection)
            //            {
            //                foreach (var z in x.GradingCollection)
            //                {
            //                    if (z.VertexName == parent.VertexName)
            //                    {
            //                        x.GradingCollection.Remove(z);
            //                        return;
            //                    }
            //                }

            //            }

            //        }
            //    }
            //}

            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}
        }

        //private void Button_Click_Save_XML_File(object sender, RoutedEventArgs e)
        //{
        //    BuiltTree();
        //}

        //private void BuiltTree()
        //{
        //    try
        //    {
        //        MyXmlSerializer.SerializeObject(GarmentTree, "XmlGradingRule.xml");

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, ex.Source);
        //    }
        //}

        private void CheckBox_CheckedStyle(object sender, RoutedEventArgs e)
        {
            
        }

        //public static IEnumerable<T> FindVisualChildren<T>(DependencyObject parent) where T : DependencyObject
        //{
        //    if (parent != null)
        //    {
        //        for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
        //        {
        //            var child = VisualTreeHelper.GetChild(parent, i);
        //            // If the available child is not null and is of required Type<T> then return with this child else continue this loop
        //            if (child != null && child is T)
        //            {
        //                yield return (T)child;
        //            }
        //            foreach (T childOfChild in FindVisualChildren<T>(child))
        //            {
        //                yield return childOfChild;
        //            }
        //        }
        //    }
        //}

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           // viewModelGradeing.ChangeVertexCommand.Execute(sender);
        }
    }
}
