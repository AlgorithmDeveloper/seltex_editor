﻿using System;
using System.Windows;
using Views.ViewModels;

namespace Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        VMMainWindow viewModelGrade = null;
        public MainWindow()
        {
            InitializeComponent();   
        }



        private void StudentViewControl_Loaded(object sender, RoutedEventArgs e)
        {
            //MVVMDemo.ViewModel.StudentViewModel studentViewModelObject =
            //   new MVVMDemo.ViewModel.StudentViewModel();
            //studentViewModelObject.LoadStudents();

            //StudentViewControl.DataContext = studentViewModelObject;
        }
        private void About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("SelteXEditor MTM solution." +
                "\n" + "Version 1.01" +
                "\n" + "all rights reserved" +
                "\n" + "Copyright © SELTAD www.seldatinc.com");
        }

    }
}
