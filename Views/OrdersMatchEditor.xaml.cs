﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Input;
using Views.ViewModels;

namespace Views
{
    /// <summary>
    /// Interaction logic for OrdersEditor.xaml
    /// </summary>
    public partial class OrdersEditor : UserControl //CustomerMeasurement
    {

        VMordersMatchEditor viewModelMatch = null;


        public OrdersEditor()
        {
            InitializeComponent();
            viewModelMatch = (VMordersMatchEditor)FindResource("VM_OME"); 
        }
        private void AddOrder_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //e.Handled = true;
            //try
            //{
            //    DialogLoadOrder dlgLoadOrder = new DialogLoadOrder();
            //    bool? result = dlgLoadOrder.ShowDialog();

            //    if (result.Value)
            //    {
            //        if (OrderCollection == null)
            //            OrderCollection = new CustomerMeasurement();
            //        else if (OrderCollection.Count > 0)
            //        {
            //            OrderCollection.Clear();
            //            baseSize = 0;
            //        }
            //        OrderCollection = dlgLoadOrder.GetNewOrders();
            //        if (OrderCollection != null)
            //        {
            //            ///***tbStyleName.Text = Path.GetFileNameWithoutExtension(mAppExcel.openFileDialog.FileName);
            //            ///*** tbSelectedSize.Text = "";
            //            //   var resultSearchGarment = OrderCollection.FirstOrDefault(searchOrder => searchOrder.OrdMeasureName == order.OrdMeasureName);
            //            // if (resultSearchGarment == null)
            //            //  {

            //            tbSelectedSize.Text = "";
            //            // Get a reference to the Order Measurement collection.
            //            CustomerMeasurement orderMeasureCollection = (CustomerMeasurement)this.TryFindResource("OrderMeasure");
            //            if (orderMeasureCollection.Count > 0)
            //            {
            //                orderMeasureCollection.Clear();
            //            }

            //            // dataGrid4.Items.Clear();
            //            //    }
            //            //**    OrderCollection.Add(order);

            //            foreach (var rec in OrderCollection)
            //            {
            //                orderMeasureCollection.Add(rec);
            //            }
            //        }
            //        else
            //        {
            //            MessageBox.Show("This Order is already exist");
            //        }
            //    }
            //    Measurements garmentMeasureCollection = (Measurements)this.Resources["GarmentMeasure"];
            //    if (garmentMeasureCollection.Count > 0)
            //    {
            //        garmentMeasureCollection.Clear();
            //    }
            //    if (_NewlistGarmentMeasure.Count > 0)
            //    {
            //        _NewlistGarmentMeasure.Clear();
            //        baseSize = 0;
            //        // dataGrid3.Items.Clear();
            //    }

            //    MeasureCollection = new Measurements();
            //    MeasureCollection = dlgLoadOrder.GetNewMeasurements();
            //    foreach (var rec in measureCollection)
            //    {
            //        garmentMeasureCollection.Add(rec);
            //    }
            //    GarmentTree = new Garments();
            //    GarmentTree = dlgLoadOrder.GetRulesLibraries();
            //    dlgLoadOrder.Close();

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}
        }

        private void FindRecommendSize_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    int orderMeasureNum = dataGrid3.Items.Count;
            //    int garmentMeasureNum = dataGrid4.Items.Count;
            //    if (orderMeasureNum != garmentMeasureNum || orderMeasureNum == 0)
            //    {
            //        MessageBox.Show("The number of measurements does not match");
            //        return;
            //    }
            //    foreach (var item in dataGrid3.Items)
            //    {
            //        Order orderMeasure = item as Order;

            //        bool isNumeric = double.TryParse(orderMeasure.OrdMeasureValue, out double n);
            //        if (!isNumeric)
            //        {
            //            MessageBox.Show("The measurement from order contains error value");
            //            return;
            //        }
            //    }
            //    bool critiHighChosen = false;
            //    CritiOrdMeasureCollection = new CustomerMeasurement();
            //    CritiGrmMeasureCollection = new RecommendSizes();
            //    foreach (var item in dataGrid3.Items)
            //    {
            //        if (CritiGrmMeasureCollection.Count == 3)
            //        {
            //            critiHighChosen = true;
            //            break;
            //        }
            //        else
            //        {
            //            int index = 0;
            //            Order orderMeasure = item as Order;
            //            //"Bust_Full" //"Waist_Full" || // "Hips_Full"
            //            if (orderMeasure.OrdMeasureName == Properties.Settings.Default.criticalMeasureHigh
            //                || orderMeasure.OrdMeasureName == Properties.Settings.Default.criticalMeasureMedium
            //                || orderMeasure.OrdMeasureName == Properties.Settings.Default.criticalMeasureLow)
            //            {
            //                CritiOrdMeasureCollection.Add(orderMeasure);
            //                index = dataGrid3.Items.IndexOf(item);
            //                RecommendSize garmentMeasure = dataGrid4.Items[index] as RecommendSize;
            //                CritiGrmMeasureCollection.Add(garmentMeasure);
            //                if (orderMeasure.OrdMeasureName == Properties.Settings.Default.criticalMeasureHigh)
            //                    critiHighChosen = true;
            //            }
            //            //"Waist_Full" 
            //            //else if ()
            //            //{
            //            //    CritiOrdMeasureCollection.Add(orderMeasure);
            //            //    index = dataGrid3.Items.IndexOf(item);
            //            //    RecommendSize garmentMeasure = dataGrid4.Items[index] as RecommendSize;
            //            //    CritiGrmMeasureCollection.Add(garmentMeasure);
            //            //}
            //            // "Hips_Full"
            //            //else if ()
            //            //{
            //            //    CritiOrdMeasureCollection.Add(orderMeasure);
            //            //    index = dataGrid3.Items.IndexOf(item);
            //            //    RecommendSize garmentMeasure = dataGrid4.Items[index] as RecommendSize;
            //            //    CritiGrmMeasureCollection.Add(garmentMeasure);
            //            //}
            //        }
            //    }
            //    if (CritiOrdMeasureCollection.Count == 0)
            //    {
            //        MessageBox.Show("Critical measure is not selected");
            //        return;
            //    }
            //    if (critiHighChosen)
            //    {
            //        ExcelParserClass mAppExcel = new ExcelParserClass();
            //        //***       mAppExcel.OpenExcelFile(DMC_FileTextBox.Text, 1);
            //        mAppExcel.ImportSizeRange<ObservableCollection<string>>(ref gradingValues, mAppExcel.excelRange);

            //        mAppExcel = new ExcelParserClass();
            //        //***          mAppExcel.OpenExcelFile(DMC_FileTextBox.Text, 1);

            //        mAppExcel.FindRecommendedZise(ref baseSize, gradingValues.Count, CritiGrmMeasureCollection, CritiOrdMeasureCollection, mAppExcel.excelRange);
            //        if (baseSize >= 0)
            //        {
            //            //mAppExcel = new ExcelParserClass();
            //            //mAppExcel.OpenExcelFile(DMC_FileTextBox.Text, 1);
            //            //mAppExcel.ImportGradingValue<ObservableCollection<string>>(ref gradingValues, mAppExcel.excelRange);
            //            tbSelectedSize.Text = gradingValues[baseSize - 3]; //baseSize.ToString();
            //            CompereDelta_Click();
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show("Critical measure " + Properties.Settings.Default.criticalMeasureHigh + " is not selected");
            //        return;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}
        }

        //private void CompereDelta_Click()
        //{
        //    int NumOfGarmentMeasure = dataGrid4.Items.Count;
        //    double ORDMeasureValue = 0.0, garmentMeasureValue = 0.0, differents = 0.0;
        //    try
        //    {
        //        ExcelParserClass mAppExcel = new ExcelParserClass();
        //        ///***       mAppExcel.OpenExcelFile(DMC_FileTextBox.Text, 1);
        //        RecomMeasureCollection = new RecommendSizes();
        //        mAppExcel.ImportRecommendedZise(ref recomMeasureCollection, baseSize, mAppExcel.excelRange);

        //        for (int i = 0; i < NumOfGarmentMeasure; i++)
        //        {
        //            Order orderMeasure = dataGrid3.Items[i] as Order;
        //            RecommendSize garmentMeasure = dataGrid4.Items[i] as RecommendSize;
        //            var select = RecomMeasureCollection.FirstOrDefault(x => x.RcmMeasureName == garmentMeasure.RcmMeasureName);
        //            garmentMeasure.RcmMeasureValue = select.RcmMeasureValue;

        //            garmentMeasure = dataGrid4.Items[i] as RecommendSize;
        //            ORDMeasureValue = Convert.ToDouble(orderMeasure.OrdMeasureValue);
        //            garmentMeasureValue = Convert.ToDouble(garmentMeasure.RcmMeasureValue);
        //            differents = (ORDMeasureValue - garmentMeasureValue);

        //            garmentMeasure.Delta = (differents > 0) ? differents.ToString("#.00") : differents.ToString("#.00");

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, ex.Source);
        //    }

        //}

        private void GenerateBatch_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    if (GarmentTree != null)
            //    {
            //        var directoryPath = AppDomain.CurrentDomain.BaseDirectory;
            //        var fileName = directoryPath + tbStyleName.Text + batchNameFile;
            //        //string directoryName = Path.GetDirectoryName(ORD_FileTextBox.Text);
            //        //directoryName = Path.GetDirectoryName(directoryName) + '\\';

            //        string directoryName = Path.GetDirectoryName(Path.GetDirectoryName(ORD_FileTextBox.Text)) + '\\',
            //               newLineComment = ";;",
            //               text = ";;---" + tbStyleName.Text + "---;;";

            //        using (StreamWriter file = new StreamWriter(fileName))
            //        {
            //            file.WriteLine(text);
            //            file.WriteLine(newLineComment);
            //            text = "@UNIT /CM";
            //            file.WriteLine(text);
            //            text = "@NEW";
            //            file.WriteLine(text);
            //            text = "@OPEN /FILE=" + directoryName + tbStyleName.Text + ".PDS";
            //            file.WriteLine(text);
            //            text = "@BASESIZE /SIZE=" + tbSelectedSize.Text;
            //            file.WriteLine(text);
            //            file.WriteLine(newLineComment);
            //            for (int o = 0; o < GarmentTree.Count; o++)
            //            {


            //                for (int p = 0; p < GarmentTree[o].MeasureCollection.Count; p++)
            //                {
            //                    int index = _NewlistGarmentMeasure.IndexOf(_NewlistGarmentMeasure.FirstOrDefault(x => x.RcmMeasureName == GarmentTree[o].MeasureCollection[p].MeasureName));
            //                    RecommendSize garmentMeasure = dataGrid4.Items[index] as RecommendSize;
            //                    double delta = Convert.ToDouble(garmentMeasure.Delta);
            //                    text = ";;;;" + GarmentTree[o].MeasureCollection[p].MeasureName + ";;;;";
            //                    file.WriteLine(text);
            //                    file.WriteLine(newLineComment);
            //                    for (int q = 0; q < GarmentTree[o].MeasureCollection[p].PieceCollection.Count; q++)
            //                    {
            //                        text = ";;" + GarmentTree[o].MeasureCollection[p].PieceCollection[q].PieceName + ";;";
            //                        file.WriteLine(text);
            //                        file.WriteLine(newLineComment);
            //                        for (int r = 0; r < GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection.Count; r++)
            //                        {
            //                            text = "@MTM /PNT=" + GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection[r].VertexName;
            //                            if (GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection[r].GradeDX != 0)
            //                                text += " /DX=" + ((GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection[r].GradeDX / 100) * delta).ToString();
            //                            if (GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection[r].GradeDY != 0)
            //                                text += " /DY=" + ((GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection[r].GradeDY / 100) * delta).ToString();
            //                            if (!string.IsNullOrWhiteSpace(GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection[r].VertexFirst.VertexName))
            //                                text += " /FRSFIX=" + GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection[r].VertexFirst.VertexName;
            //                            if (!string.IsNullOrWhiteSpace(GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection[r].VertexLast.VertexName))
            //                                text += " /LSFIX=" + GarmentTree[o].MeasureCollection[p].PieceCollection[q].GradingCollection[r].VertexLast.VertexName;
            //                            file.WriteLine(text);
            //                            file.WriteLine(newLineComment);
            //                        }
            //                    }
            //                }
            //            }
            //            file.WriteLine(newLineComment);
            //            text = "@SAVE /FILE=" + directoryName + tbStyleName.Text + "_new" + ".PDS";
            //            file.WriteLine(text);
            //            text = "@EXIT";
            //            file.WriteLine(text);
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show("Is not exist a grading rules");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}

        }

        private void DMCMoveUp_Click(object sender, RoutedEventArgs e)
        {
            viewModelMatch.GarmentMoveUpCommand.Execute(sender);
            //RecommendSize garmentMeasure = ((FrameworkElement)sender).DataContext as RecommendSize;
            //int sel = _NewlistGarmentMeasure.IndexOf(garmentMeasure);
            //if (sel > 0)
            //{
            //    _NewlistGarmentMeasure.RemoveAt(sel);
            //    _NewlistGarmentMeasure.Insert(sel - 1, garmentMeasure);
            //}
            // dataGrid4.Items.Refresh();
        }

        private void DMCMoveDown_Click(object sender, RoutedEventArgs e)
        {
            viewModelMatch.GarmentMoveDownCommand.Execute(sender);
            //RecommendSize garmentMeasure = ((FrameworkElement)sender).DataContext as RecommendSize;
            //int sel = _NewlistGarmentMeasure.IndexOf(garmentMeasure);
            //if (sel < _NewlistGarmentMeasure.Count - 1)
            //{
            //    _NewlistGarmentMeasure.RemoveAt(sel);
            //    _NewlistGarmentMeasure.Insert(sel + 1, garmentMeasure);
            //}
            //dataGrid4.Items.Refresh();
        }

        private void OrdMoveUp_Click(object sender, RoutedEventArgs e)
        {
            viewModelMatch.OrdMoveUpCommand.Execute(sender);

            //if ((sender as FrameworkElement).DataContext is Order orderMeasure)
            //{
            //    int sel = _NewlistOrderMeasure.IndexOf(orderMeasure);
            //    if (sel > 0)
            //    {
            //        _NewlistOrderMeasure.RemoveAt(sel);
            //        _NewlistOrderMeasure.Insert(sel - 1, orderMeasure);
            //    }
            //    dataGrid3.Items.Refresh();
            //}
        }

        private void OrdMoveDown_Click(object sender, RoutedEventArgs e)
        {
            viewModelMatch.OrdMoveDownCommand.Execute(sender);

            //if (((FrameworkElement)sender).DataContext is Order orderMeasure)
            //{
            //    int sel = _NewlistOrderMeasure.IndexOf(orderMeasure);
            //    if (sel < _NewlistOrderMeasure.Count - 1)
            //    {
            //        _NewlistOrderMeasure.RemoveAt(sel);
            //        _NewlistOrderMeasure.Insert(sel + 1, orderMeasure);
            //    }
            //    dataGrid3.Items.Refresh();
            //}
        }

        private void CheckBox_CheckedStyle(object sender, RoutedEventArgs e)
        {

        }



        #region test

        //private void SelectGarmentMeasure(object sender, RoutedEventArgs e)
        //{
        //    //ToggleButton myButton = (sender as ToggleButton);
        //    //myButton.Content = myButton.IsChecked.Value ? "Selected" : "UnSelected";

        //    //Measurement garmentMeasure = ((FrameworkElement)sender).DataContext as Measurement;
        //    //RecommendSize recommendSize = new RecommendSize()
        //    //{
        //    //    RcmMeasureID = garmentMeasure.MeasureID,
        //    //    RcmMeasureName = garmentMeasure.MeasureName,
        //    //    Selected = myButton.IsChecked.Value ? true : false
        //    //};
        //    //var measure = _NewlistGarmentMeasure.FirstOrDefault(x => x.RcmMeasureName == recommendSize.RcmMeasureName);
        //    //if (measure == null)
        //    //    _NewlistGarmentMeasure.Add(recommendSize);
        //    //else
        //    //    _NewlistGarmentMeasure.Remove(measure);

        //    //dataGrid4.Items.Refresh();
        //}

        //private void SelectOrderMeasure(object sender, RoutedEventArgs e)
        //{
        //    //ToggleButton myButton = (sender as ToggleButton);
        //    //myButton.Content = myButton.IsChecked.Value ? "Selected" : "Unselected";

        //    //Order orderMeasure = ((FrameworkElement)sender).DataContext as Order;
        //    //orderMeasure.OrdSelected = !orderMeasure.OrdSelected;
        //    //if (orderMeasure.OrdSelected)
        //    //    _NewlistOrderMeasure.Add(orderMeasure);
        //    //else
        //    //    _NewlistOrderMeasure.Remove(orderMeasure);
        //    //dataGrid3.Items.Refresh();
        //    // object ID = ((Button)sender).CommandParameter;
        //}

        //private void ToggleButton_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    viewModelMatch.SelectMeasureFromOrderCommand.Execute(sender);
        //}






        //private void SelectOrderMeasure1(object sender, RoutedEventArgs e)
        //{
        //    ToggleButton myButton = (sender as ToggleButton);
        //    myButton.Content = myButton.IsChecked.Value ? "Selected" : "Unselected";

        //    Order orderMeasure = ((FrameworkElement)sender).DataContext as Order;
        //    orderMeasure.Selected = !orderMeasure.Selected;
        //    if (orderMeasure.Selected)
        //        _NewlistOrderMeasure.Add(orderMeasure);
        //    else
        //        _NewlistOrderMeasure.Remove(orderMeasure);
        //    dataGrid3.Items.Refresh();
        //    // object ID = ((Button)sender).CommandParameter;
        //}
        //private void BtnOpenDMC_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        ExcelParserClass mAppExcel = new ExcelParserClass();

        //        mAppExcel.BrowseExcelDialog();
        //        if (mAppExcel.openFileDialog.FileName.Length > 0)
        //        {
        //            //mAppExcel.OpenExcelFile(mAppExcel.openFileDialog.FileName, 1);
        //            //DMC_FileTextBox.Text = mAppExcel.openFileDialog.FileName;
        //            //tbStyleName.Text = Path.GetFileNameWithoutExtension(mAppExcel.openFileDialog.FileName);
        //            tbSelectedSize.Text = "";
        //        }
        //        if (mAppExcel.excelSheet != null && mAppExcel.excelRange != null)
        //        {
        //            // Get a reference to the Garment Measurement collection. 
        //            Measurements garmentMeasureCollection = (Measurements)this.Resources["GarmentMeasure"];
        //            if (garmentMeasureCollection.Count > 0)
        //            {
        //                garmentMeasureCollection.Clear();
        //            }
        //            if (_NewlistGarmentMeasure.Count > 0)
        //            {
        //                _NewlistGarmentMeasure.Clear();
        //                baseSize = 0;
        //                // dataGrid3.Items.Clear();
        //            }
        //            // Generate some Garment data and add it to the  ObservableCollection.
        //            mAppExcel.ImportMeasure<Measurements>(ref measureCollection, mAppExcel.excelRange);
        //            foreach (var rec in measureCollection)
        //            {
        //                garmentMeasureCollection.Add(rec);
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, ex.Source);
        //    }

        //}

        //private void BtnOpenRules_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        //OpenFileDialog openFileDialog = new OpenFileDialog()
        //        //{
        //        //    // Allow multiple file selection.              
        //        //    Multiselect = false,
        //        //    DefaultExt = ".xml",
        //        //    Filter = "Rule files (*.xml)|*.xml",
        //        //    // Set the text for the title bar.
        //        //    Title = "Select Rule File "
        //        //};
        //        //openFileDialog.ShowDialog();
        //        //if (openFileDialog.FileName.Length > 0)
        //        //{
        //        ///***          Rules_FilesTextBox.Text = openFileDialog.SafeFileName;
        //        //    GarmentTree = new Garments();
        //        //    MyXmlSerializer.DeserializeObject(ref GarmentTree, openFileDialog.FileName);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, ex.Source);
        //    }
        //}

        //private void LoadedUnit(object sender, RoutedEventArgs e)
        //{
        //    this.tbUnit.Text = Properties.Settings.Default.unitWorker;
        //}
        //private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    Properties.Settings.Default.unitWorker = this.tbUnit.Text;
        //    Properties.Settings.Default.Save();
        //}
        //private void UngroupButton_Click(object sender, RoutedEventArgs e)
        //{
        //    ICollectionView collection = CollectionViewSource.GetDefaultView(dgOrderMeasure.ItemsSource);
        //    if (collection != null)
        //    {
        //        collection.GroupDescriptions.Clear();
        //    }
        //}

        //private void GroupButton_Click(object sender, RoutedEventArgs e)
        //{
        //    ICollectionView collection = CollectionViewSource.GetDefaultView(dgOrderMeasure.ItemsSource);
        //    if (collection != null && collection.CanGroup == true)
        //    {
        //        collection.GroupDescriptions.Clear();
        //        collection.GroupDescriptions.Add(new PropertyGroupDescription("Selected"));
        //    }
        //}
        #endregion


    }
    public class ConvertItemToIndex : IValueConverter
    {
        #region IValueConverter Members
        //Convert the Item to an Index
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                DataGridRow row = value as DataGridRow;
                if (row == null)
                    throw new InvalidOperationException("This converter class can only be used with DataGridRow elements.");

                return row.GetIndex() + 1;
            }
            catch (Exception e)
            {
                throw new NotImplementedException(e.Message);
            }
        }
        //One way binding, so ConvertBack is not implemented
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
    [ValueConversion(typeof(Boolean), typeof(String))]
    public class CompleteConverter : IValueConverter
    {
        // This converter changes the value of a Tasks Complete status from true/false to a string value of
        // "Matching"/"Not Matching" for use in the row group header.
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool matching = (bool)value;
            if ((matching))
                return "Selected";
            else
                return "Not Selected";
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //string strComplete = (string)value;
            //if (strComplete == "Needed")
            //    return true;
            //else
            //    return false;
            return parameter;
        }
    }
}

