﻿using System;
using System.Linq;
using System.Windows;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;

using mExcel = Microsoft.Office.Interop.Excel;
using System.Collections.ObjectModel;

using Models;

namespace Views.Utilities
{
    class ExcelParserClass ///int index = myList.FindIndex(a => a.Prop == oProp);
    {
        public mExcel.Application excelApp = null;
        public mExcel.Workbook excelBook = null;
        public mExcel.Worksheet excelSheet = null;
        public mExcel.Range excelRange = null;
        public OpenFileDialog openFileDialog = null;

        public ExcelParserClass()
        {
            excelApp = new mExcel.Application(); // Create an Object Of Excel Application
                                                 //if (excelRange != null) Marshal.ReleaseComObject(excelRange);
                                                 //if (excelSheet != null) Marshal.ReleaseComObject(excelSheet);
                                                 ////if (sheets != null) Marshal.ReleaseComObject(sheets);
                                                 //if (excelBook != null) Marshal.ReleaseComObject(excelBook);
                                                 ////if (books != null) Marshal.ReleaseComObject(books);
                                                 //if (excelApp != null) Marshal.ReleaseComObject(excelApp);
        }

        public void BrowseExcelDialog()
        {
            try
            {
                // Create the dialog box object.
                openFileDialog = new OpenFileDialog()
                {

                    // Allow multiple file selection.
                    Multiselect = false,

                    // Set the text for the title bar.
                    Title = "Choice One File",


                    Filter = "Excel Files|(.xls);*.xls;(.xlsx);*.xlsx"

                    // Do not verify that the file exists.
                    //this.openFileDialog.CheckFileExists = false;

                    // Do verify that the path exists.
                    // this.openFileDialog.CheckPathExists = true;

                    // Add a default extension if the user does not type one.
                    //this.openFileDialog.AddExtension = true;

                    // Set the default extension.
                    //this.openFileDialog.DefaultExt = ".xlsx";

                    //FileDialogCustomPlace uy = new FileDialogCustomPlace(@"C:\Users");
                    // openFileDialog.CustomPlaces.Add(uy);
                    // ValidateNames = true AddExtension: DefaultExt: DereferenceLinks: FileName: InitialDirectory: Multiselect: Filter FilterIndex:
                    //ValidateNames: 

                    //    openFileDialog1.InitialDirectory = "c:\\" ;              
                    //openFileDialog1.FilterIndex = 2;
                    //openFileDialog1.RestoreDirectory = true;



                };
                openFileDialog.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        public void OpenExcelFile(string Location, int workSheet)
        {
            try
            {
                excelBook = excelApp.Workbooks.Open(Location);
                excelSheet = (mExcel.Worksheet)excelBook.Sheets[workSheet];
                excelRange = excelSheet.UsedRange;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        public int FindRecommendedZise(int _sizeRange, RecommendSizes _garmentMeasureCollection,
           CustomersMeasurement _ordMeasureCollection, Range _localEexcelRange) // ref int _baseSize, 
        {
            int baseSize = -1;
            try
            {
                #region Define local variables    
                string strCellData = string.Empty;
                int columnLocation = 2, rowLocation = 0, sizeRange = _sizeRange + columnLocation,
                index = _ordMeasureCollection.IndexOf(_ordMeasureCollection.FirstOrDefault(x => x.CstmrMeasureName == Properties.Settings.Default.criticalMeasureHigh));
                double douCellDataCurrent = 0.0, douCellDataNext = 0.0,
                critiMeasureValue = Convert.ToDouble(_ordMeasureCollection[index].CstmrMeasureValue);
                #endregion

                #region find recommended size for costumer limit tolerance value

                if (FindStrLocation(ref rowLocation, columnLocation, _garmentMeasureCollection[index].MeasureName, _localEexcelRange))
                {
                    douCellDataCurrent = (_localEexcelRange.Cells[rowLocation, 3] as Range).Value2;
                    if (douCellDataCurrent - critiMeasureValue > Properties.Settings.Default.ToleranceLimit)
                    {
                        MessageBox.Show("The critical value is too small");
                        return baseSize;
                    }
                    douCellDataCurrent = (_localEexcelRange.Cells[rowLocation, columnLocation + _sizeRange] as Range).Value2;
                }
                if (critiMeasureValue - douCellDataCurrent > Properties.Settings.Default.ToleranceLimit)
                {
                    MessageBox.Show("The critical value is too large");
                    return baseSize;
                }
                for (int colCnt = 3; colCnt <= columnLocation + _sizeRange + 1; colCnt++)// _localEexcelRange.Columns.Count
                {
                    if (colCnt > columnLocation + _sizeRange)
                    {
                        MessageBox.Show("The critical value is out of Tolerance");
                        return baseSize;
                    }
                    douCellDataCurrent = 0.0;
                    douCellDataNext = 0.0;
                    douCellDataCurrent = (_localEexcelRange.Cells[rowLocation, colCnt] as Range).Value2;
                    if (colCnt < columnLocation + _sizeRange)
                    {
                        douCellDataNext = (_localEexcelRange.Cells[rowLocation, colCnt + 1] as Range).Value2;
                    }
                    if (Math.Abs((critiMeasureValue - douCellDataCurrent)) < Properties.Settings.Default.ToleranceLimit &&
                        Math.Abs((critiMeasureValue - douCellDataNext)) > Properties.Settings.Default.ToleranceLimit
                        || Math.Abs((critiMeasureValue - douCellDataCurrent)) < Properties.Settings.Default.ToleranceLimit &&
                        Math.Abs((critiMeasureValue - douCellDataCurrent)) < Math.Abs((critiMeasureValue - douCellDataNext)))
                    {
                        baseSize = colCnt;
                        break;
                    }
                    else if (Math.Abs((critiMeasureValue - douCellDataCurrent)) < Properties.Settings.Default.ToleranceLimit &&
                             Math.Abs((critiMeasureValue - douCellDataNext)) < Properties.Settings.Default.ToleranceLimit &&
                             Math.Abs((critiMeasureValue - douCellDataCurrent)) == Math.Abs((critiMeasureValue - douCellDataNext)))
                    {
                        if (_garmentMeasureCollection.Count >= 2)
                        {
                            index = _ordMeasureCollection.IndexOf(_ordMeasureCollection.FirstOrDefault(x => x.CstmrMeasureName == Properties.Settings.Default.criticalMeasureMedium));
                            critiMeasureValue = Convert.ToDouble(_ordMeasureCollection[index].CstmrMeasureValue);

                            if (FindStrLocation(ref rowLocation, columnLocation, _garmentMeasureCollection[index].MeasureName, _localEexcelRange))
                            {
                                douCellDataCurrent = 0.0;
                                douCellDataNext = 0.0;
                                douCellDataCurrent = (_localEexcelRange.Cells[rowLocation, colCnt] as Range).Value2;
                                douCellDataNext = (_localEexcelRange.Cells[rowLocation, colCnt + 1] as Range).Value2;
                                if (Math.Abs((critiMeasureValue - douCellDataCurrent)) < Math.Abs((critiMeasureValue - douCellDataNext)))
                                {
                                    baseSize = colCnt;
                                    break;
                                }
                                else if (Math.Abs((critiMeasureValue - douCellDataCurrent)) > Math.Abs((critiMeasureValue - douCellDataNext)))
                                {
                                    baseSize = colCnt + 1;
                                    break;
                                }
                                else
                                {
                                    if (_garmentMeasureCollection.Count > 2)
                                    {
                                        index = _ordMeasureCollection.IndexOf(_ordMeasureCollection.FirstOrDefault(x => x.CstmrMeasureName == Properties.Settings.Default.criticalMeasureLow));
                                        critiMeasureValue = Convert.ToDouble(_ordMeasureCollection[index].CstmrMeasureValue);

                                        if (FindStrLocation(ref rowLocation, columnLocation, _garmentMeasureCollection[index].MeasureName, _localEexcelRange))
                                        {
                                            douCellDataCurrent = 0.0;
                                            douCellDataNext = 0.0;
                                            douCellDataCurrent = (_localEexcelRange.Cells[rowLocation, colCnt] as Range).Value2;
                                            douCellDataNext = (_localEexcelRange.Cells[rowLocation, colCnt + 1] as Range).Value2;
                                            if (Math.Abs((critiMeasureValue - douCellDataCurrent)) < Math.Abs((critiMeasureValue - douCellDataNext)))
                                            {
                                                baseSize = colCnt;
                                                break;
                                            }
                                            else if (Math.Abs((critiMeasureValue - douCellDataCurrent)) > Math.Abs((critiMeasureValue - douCellDataNext)))
                                            {
                                                baseSize = colCnt + 1;
                                                break;
                                            }
                                            else
                                            {
                                                MessageBox.Show("Can't find recommended size, missing #3 critical measurements");
                                                return baseSize;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Can't find recommended size, all critical measurements is equal");
                                        return baseSize;
                                    }                                    //Properties.Settings.Default.criticalMeasureMedium
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Can't find recommended size, missing #2 critical measurements");
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
                #endregion find recommended size
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
            finally
            {
                ReleaseObject();
            }
            return baseSize;
        }

        public void ImportSizeRange(ref ObservableCollection<string> _gradeCollection, Range _localEexcelRange)
        {
            try
            {
                #region Define local variables
                ObservableCollection<string> listOfGrading = new ObservableCollection<string>();
                string strCellData = string.Empty;
                int columnLocation = 2, rowLocation = 0;
                #endregion

                #region Create list of grading
                if (FindStrLocation(ref rowLocation, columnLocation, "Measure", _localEexcelRange))
                {
                    for (int colCnt = 3; colCnt <= _localEexcelRange.Columns.Count; colCnt++)
                    {
                        strCellData = "";
                        strCellData = (string)(_localEexcelRange.Cells[rowLocation, colCnt] as Range).Value2;
                        if (strCellData != null)
                            listOfGrading.Add(strCellData);
                    }
                }
                _gradeCollection = listOfGrading;

                #endregion Create list of grading
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
            finally
            {
                ReleaseObject();
            }
        }

        public void ImportRecommendedZise(ref RecommendSizes _garmentMeasureCollection, int _baseSize, Range _localEexcelRange)
        {
            try
            {
                #region Define local variables
                RecommendSizes listOfBaseZise = new RecommendSizes();
                string strCellData = string.Empty;
                int columnLocation = 2, rowLocation = 0;
                double douCellData = 0.0;
                #endregion

                #region Create list of measurements
                if (FindStrLocation(ref rowLocation, columnLocation, "Measure", _localEexcelRange))
                {
                    for (int i = 0; rowLocation < _localEexcelRange.Rows.Count - 1 && i < _garmentMeasureCollection.Count; rowLocation++)
                    {                        
                        strCellData = "";
                        strCellData = (string)(_localEexcelRange.Cells[rowLocation + 1, 2] as Range).Value2;
                        if (strCellData != null)
                        {
                            var index = _garmentMeasureCollection.IndexOf(_garmentMeasureCollection.FirstOrDefault(measure => measure.MeasureName == strCellData));
                            if (index >= 0)
                            {                                
                                douCellData = 0.0;
                                douCellData = (_localEexcelRange.Cells[rowLocation + 1, _baseSize] as Range).Value2;
                                _garmentMeasureCollection[index].RcmMeasureValue = douCellData;
                                i++;
                            }
                        }
                    }
                }
               // _garmentMeasureCollection = listOfBaseZise;
                #endregion Create list of measurements              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
            finally
            {
                ReleaseObject();
            }
        }

        public void ImportMeasure(ref Measurements _measureCollection, Range _localEexcelRange)
        {
            try
            {
                #region Define local variables 
                string strMeasureCell = string.Empty;
                int columnLocation = 2, rowLocation = 0;
                #endregion

                #region Create list of measurements
                if (FindStrLocation(ref rowLocation, columnLocation, "Measure", _localEexcelRange))
                {
                    for (rowLocation += 1; rowLocation < _localEexcelRange.Rows.Count; rowLocation++)
                    {
                        strMeasureCell = "";
                        strMeasureCell = (string)(_localEexcelRange.Cells[rowLocation, columnLocation] as mExcel.Range).Value2;
                        if (strMeasureCell != null)
                        {
                            _measureCollection.Add(new Measurement { MeasureName = strMeasureCell });
                        }
                    }
                }
                #endregion Create list of measurements
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
            finally
            {
                ReleaseObject();
            }
        }

        public void ImportRules<ObservableCollection>(ref RulesLibraries _rulesCollection, Range _localEexcelRange)// _localEexcelRange
        {
            try
            {
                #region Define local variables                
                ObservableCollection<Vertex> listOfVertex = new ObservableCollection<Vertex>();
                RulesLibraries listOfRules = new RulesLibraries();
                Piece tempPiece = null;
                // Vertex tempPoint = null;
                RuleLibrary tempRuleLibrary = null;
                string strCellData = string.Empty, strRuleCell = string.Empty, strPointCell = string.Empty;
                int columnLocation = 1, rowLocation = 0;
                bool findAnotherRule = false;
                #endregion

                #region Create list of Rules & points
                if (FindStrLocation(ref rowLocation, columnLocation, "Rule", _localEexcelRange))
                {
                    for (rowLocation += 2; rowLocation <= _localEexcelRange.Rows.Count; rowLocation++) //  loop find all rule (piece)
                    {
                        strRuleCell = (string)(_localEexcelRange.Cells[rowLocation, columnLocation] as mExcel.Range).Value2;
                        if (strRuleCell != null || rowLocation == _localEexcelRange.Rows.Count) // find the first rule (piece) || last rule
                        {
                            if (findAnotherRule == false)
                            {
                                findAnotherRule = true;
                                tempPiece = new Piece()
                                {
                                    PieceName = strRuleCell
                                };//0, strRuleCell
                                strRuleCell = "";
                            }
                            else
                            {
                                findAnotherRule = false;
                                ///%%%
                                tempRuleLibrary = new RuleLibrary()
                                {
                                    MyPiece = tempPiece
                                };
                                foreach (var item in listOfVertex)
                                {
                                    tempRuleLibrary.VertexCollection.Add(item);
                                }
                                ///%%%


                                //tempRuleLibrary = new RuleLibrary()
                                //{
                                //    MyPiece = tempPiece
                                //};
                                ////tempPieces = new Pieces(tempPiece, listOfPoints);
                                //foreach (var item in listOfVertex)
                                //{
                                //    //var clone = (RateModel)item.Clone();
                                //    tempRuleLibrary.VertexCollection.Add(item);
                                //}


                                // tempPieces.MyPiece = tempPiece;
                                // tempPieces.PointsCollection.= listOfPoints;
                                listOfRules.Add(tempRuleLibrary);
                                strRuleCell = "";
                                listOfVertex.Clear();
                                if (rowLocation < _localEexcelRange.Rows.Count)
                                {
                                    rowLocation--;
                                    continue;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                        strPointCell = (string)(_localEexcelRange.Cells[rowLocation, columnLocation + 1] as mExcel.Range).Value2; // read first point for above piece
                        if (strPointCell != null)
                        {
                            //tempPoint = new Vertex()
                            //{
                            //    VertexName = strPointCell
                            //};
                            listOfVertex.Add(new Vertex { VertexName = strPointCell });
                            strPointCell = "";
                        }
                    }
                    _rulesCollection = listOfRules;
                }
                #endregion Create list of Rules & points
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
            finally
            {
                ReleaseObject();
            }
        }

        private void ReleaseObject(object obj = null)
        {
            try
            {
                if (excelRange != null) Marshal.ReleaseComObject(excelRange);
                if (excelSheet != null) Marshal.ReleaseComObject(excelSheet);
                //if (sheets != null) Marshal.ReleaseComObject(sheets);
                if (excelBook != null) Marshal.ReleaseComObject(excelBook);
                // if (books != null) Marshal.ReleaseComObject(books);
                //Marshal.ReleaseComObject(obj);
                //obj = null;
                excelApp.Quit();
                if (excelApp != null) Marshal.ReleaseComObject(excelApp);
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private bool FindStrLocation(ref int _rowLocation, int _columnLocation, string _strSearch, Range _localEexcelRange)
        {
            try
            {
                #region Define local variables           
                string strCellData = string.Empty;
                #endregion
                for (int rowCnt = 1; rowCnt <= _localEexcelRange.Rows.Count; rowCnt++)
                {
                    strCellData = (string)(_localEexcelRange.Cells[rowCnt, _columnLocation] as mExcel.Range).Value2;
                    if (strCellData == _strSearch)
                    {
                        _rowLocation = rowCnt;
                        return true;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
            return false;
        }


        //public void ImportData<ObservableCollection>(Range LocalEexcelRange, DataGrid dg)
        //{
        //    try
        //    {
        //        #region Define local variables
        //        System.Data.DataTable dt = new System.Data.DataTable();
        //        ObservableCollection<String> listOfMeasure = new ObservableCollection<string>();
        //        string strColumnData = string.Empty, strRowsData = string.Empty, strCellData = string.Empty;
        //        double douCellData;
        //        #endregion

        //        #region CreateHeader
        //        //for (int colCnt = 1; colCnt <= LocalEexcelRange.Columns.Count; colCnt++)
        //        //{
        //        //    strColumnData = "";
        //        //    strColumnData = (string)(LocalEexcelRange.Cells[1, colCnt] as mExcel.Range).Value2;
        //        //    dt.Columns.Add(strColumnData, typeof(string));
        //        //}
        //        #endregion CreateHeader

        //        #region CreateTeble (col & row)    
        //        for (int rowCnt = 1; rowCnt <= LocalEexcelRange.Rows.Count; rowCnt++)
        //        {
        //            strRowsData = "";
        //            for (int colCnt = 1; colCnt <= LocalEexcelRange.Columns.Count; colCnt++)
        //            {
        //                try
        //                {
        //                    strCellData = (string)(LocalEexcelRange.Cells[rowCnt, colCnt] as mExcel.Range).Value2;
        //                    strRowsData += strCellData + "|";
        //                }
        //                catch (Exception)
        //                {
        //                    douCellData = (LocalEexcelRange.Cells[rowCnt, colCnt] as mExcel.Range).Value2;
        //                    strRowsData += douCellData.ToString() + "|";
        //                }
        //            }
        //            strRowsData = strRowsData.Remove(strRowsData.Length - 1, 1);
        //            dt.Rows.Add(strRowsData.Split('|'));
        //        }
        //        dg.ItemsSource = dt.DefaultView;
        //        #endregion CreateTeble (col & row) 
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, ex.Source);
        //    }
        //    finally
        //    {
        //        ReleaseObject();
        //    }
        //}

        //public void ResetDatagrid(DataGrid dg)
        //{
        //    if (dg.ItemsSource != null)
        //    {
        //        try
        //        {
        //            dg.ItemsSource = "";
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message, ex.Source);
        //        }
        //        finally
        //        {
        //            releaseObject();
        //        }
        //    }
        //    else
        //    {

        //        MessageBox.Show("No data in view", "Error reset action", MessageBoxButton.OK, MessageBoxImage.Asterisk, MessageBoxResult.No, MessageBoxOptions.ServiceNotification);
        //        //MessageBox.Show("No data in view", "Error reset action", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.No, MessageBoxOptions.ServiceNotification);
        //        //MessageBox.Show("No data in view", "Error reset action", MessageBoxButton.OK, MessageBoxImage.Exclamation, MessageBoxResult.None, MessageBoxOptions.None);
        //        //MessageBox.Show("No data in view", "Error reset action", MessageBoxButton.OK, MessageBoxImage.Hand, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
        //        //MessageBox.Show("No data in view", "Error reset action", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.No, MessageBoxOptions.ServiceNotification);
        //        //MessageBox.Show("No data in view", "Error reset action", MessageBoxButton.OK, MessageBoxImage.None, MessageBoxResult.No, MessageBoxOptions.ServiceNotification);
        //        //MessageBox.Show("No data in view", "Error reset action", MessageBoxButton.OK, MessageBoxImage.Question, MessageBoxResult.No, MessageBoxOptions.ServiceNotification);
        //        //MessageBox.Show("No data in view", "Error reset action", MessageBoxButton.OK, MessageBoxImage.Stop , MessageBoxResult.No, MessageBoxOptions.ServiceNotification);

        //        //MessageBox.Show("No data in view", "Error reset action", MessageBoxButton.OK, MessageBoxImage.Warning, MessageBoxResult.No, MessageBoxOptions.ServiceNotification);
        //        //  MessageBox.Show("No data in view", "Error reset action");
        //    }

        //}

        //public void ExportDataToExcel(DataGrid dg)
        //{
        //    if (dg.ItemsSource != null)
        //    {
        //        try
        //        {
        //            dg.SelectAllCells();
        //            dg.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
        //            ApplicationCommands.Copy.Execute(null, dg);
        //            String result = (string)Clipboard.GetData(DataFormats.Text);
        //            dg.UnselectAllCells();
        //            StreamWriter sw = new StreamWriter(@"C:\Users\developer\Desktop\test.xls");
        //            //StreamWriter sw1 = new StreamWriter(Stream.Null, System.Text.Encoding.UTF8);
        //            // StreamWriter sw = new StreamWriter(@"C:\Users\developer\Desktop\test.csv", true, System.Text.Encoding.UTF8);
        //            //StreamWriter sw = new StreamWriter(@"C:\Users\developer\Desktop\test.xls", true, System.Text.Encoding.UTF8);
        //            sw.WriteLine(result.Replace(',', ' '));
        //            sw.Close();
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show(ex.Message, ex.Source);
        //        }
        //        finally
        //        {
        //            releaseObject();
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("No data in view", "Error reset action");
        //    }
        //}


    }
}

