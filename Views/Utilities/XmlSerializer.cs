﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Windows;

namespace Views.Utilities
{
    static public class MyXmlSerializer
    {

        ////Write a Comment node.
        //writer.WriteComment("sample XML");
        static public void SerializeObject<T>(T _gradingRulesToXML, string _filePath) // Tops // static public void SerializeObject(GradingRulesToXMLFile _gradingRulesToXML, string _filePath)
        {
            try
            {
                // Create an instance of the XmlSerializer specifying type and namespace.
                XmlSerializer serializer = new XmlSerializer(typeof(T)); //  XmlSerializer serializer = new XmlSerializer(typeof(GradingRulesToXMLFile));

                //var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "//SerializationOverview.xml";
                //StreamWriter writer1 = File.CreateText(path);
                // StreamWriter
                //TextWriter writer = new StreamWriter(filename);
                //serializer.Serialize(writer, /*"any class type for Serialize"*/);
                //writer.Close();

                //  Since we have to serialize in a file we create a "TextWriter".
                // Since TextWriter implements IDisposable, we used using so that we need not close the writer.  
                using (StreamWriter writer = new StreamWriter(_filePath))
                {
                    serializer.Serialize(writer, _gradingRulesToXML);
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
                //check here why it failed and ask user to retry if the file is in use.
            }
        }

        static public void DeserializeObject<T>(ref T _objectDeserialize, string _filePath)
        {
            try
            {
                // Create an instance of the XmlSerializer specifying type and namespace.            
                XmlSerializer deserializer = new XmlSerializer(typeof(T));

                /* If the XML document has been altered with unknown 
                nodes or attributes, handle them with the 
                UnknownNode and UnknownAttribute events.*/
                //serializer.UnknownNode += new
                //XmlNodeEventHandler(serializer_UnknownNode);
                //serializer.UnknownAttribute += new
                //XmlAttributeEventHandler(serializer_UnknownAttribute);



                using (FileStream fileStream = new FileStream(_filePath, FileMode.Open))
                {
                    if (fileStream.Length == 0)
                    {
                        return;
                    }

                    XmlReader reader = XmlReader.Create(fileStream);
                    if (reader.IsStartElement())
                    {
                        _objectDeserialize = (T)deserializer.Deserialize(reader);
                    }
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
                //check here why it failed and ask user to retry if the file is in use.
            }





        }

        //private void serializer_UnknownNode(object sender, XmlNodeEventArgs e)
        //{
        //    Console.WriteLine("Unknown Node:" + e.Name + "\t" + e.Text);
        //}

        //private void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        //{
        //    System.Xml.XmlAttribute attr = e.Attr;
        //    Console.WriteLine("Unknown attribute " +
        //    attr.Name + "='" + attr.Value + "'");
        //}
    }
}
