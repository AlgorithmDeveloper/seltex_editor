﻿using System;
using System.Windows;
using Views.Utilities;
using System.ComponentModel;

namespace Views.ViewModels
{

    class VMMainWindow
    {
        public RelayCommand CloseWindowCommand { get; set; } //CloseWindow

        public VMMainWindow()
        {
            if (CloseWindowCommand == null)
                CloseWindowCommand = new RelayCommand(CloseWindow);
            Application.Current.MainWindow.Closing += new CancelEventHandler(MainWindow_Closing);
        }



        private void CloseWindow(object sender)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("Are you sure to exit SelteXEditor application ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                   // SaveToXML(sender);
                    Application.Current.Shutdown();
                }
                else
                {
                    CancelEventArgs e = new CancelEventArgs
                    {
                        Cancel = true
                   };
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("Are you sure to exit SelteXEditor application ?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    // SaveToXML(sender);
                    Application.Current.Shutdown();
                }
                else
                {
                    e.Cancel = true;
                  
                    // return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }
    }

}
