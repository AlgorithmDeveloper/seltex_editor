﻿using Models;
using Views.Utilities;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Views.ViewModels
{

    class VMgradeRulesEditor : BaseModel
    {


        private Garments garmentToShow = null;
        public Garments GarmentToShow
        {
            get
            {
                if (garmentToShow == null)
                    garmentToShow = new Garments();
                return garmentToShow;
            }
            set { SetPropertry(ref garmentToShow, value); }
        }

        private Garments gradingRulesTree = null;
        public Garments GradingRulesTree
        {
            get
            {
                if (gradingRulesTree == null)
                    gradingRulesTree = new Garments();
                return gradingRulesTree;
            }
            set { SetPropertry(ref gradingRulesTree, value); }
        }

        private GarmentsInfo garmentList = null;
        public GarmentsInfo GarmentList
        {
            get
            {
                if (garmentList == null)
                    garmentList = new GarmentsInfo();
                return garmentList;
            }
            set { SetPropertry(ref garmentList, value); }

        }

        private GarmentInfo garmentSelected;
        public GarmentInfo GarmentSelected
        {
            get { return garmentSelected; }
            set { SetPropertry(ref garmentSelected, value); }
        }




        //private Measurements vmMeasureCollection = null;
        //public Measurements VmMeasureCollection
        //{
        //    get
        //    {
        //        if (vmMeasureCollection == null)
        //            vmMeasureCollection = new Measurements();
        //        return vmMeasureCollection;
        //    }
        //    set
        //    {
        //        if (Equals(value, vmMeasureCollection)) return;
        //        vmMeasureCollection = value;
        //        NotifyPropertyChanged();
        //    }
        //}
        //private RulesLibraries vmRuleCollection = null;
        //public RulesLibraries VmRuleCollection
        //{
        //    get
        //    {
        //        if (vmRuleCollection == null)
        //            vmRuleCollection = new RulesLibraries();
        //        return vmRuleCollection;
        //    }
        //    set
        //    {
        //        if (Equals(value, vmRuleCollection)) return;
        //        vmRuleCollection = value;
        //        NotifyPropertyChanged();
        //    }
        //}


        public RelayCommand AddStyleCommand { get; set; }
        public RelayCommand AddMeasureCommand { get; set; }
        public RelayCommand DeleteMeasureCommand { get; set; }
        public RelayCommand AddPieceCommand { get; set; }
        public RelayCommand DeletePieceCommand { get; set; }
        public RelayCommand AddVertexCommand { get; set; }
        public RelayCommand DeleteVertexCommand { get; set; }
        public RelayCommand ChangeVertexCommand { get; set; }
        public RelayCommand SelectedGarmentCommand { get; set; }
        public RelayCommand SaveXMLCommand { get; set; }
     
        public RelayCommand CancelGradeRuleCommand { get; set; }
        //   public DelegateCommand<object> MyCommand { get; private set; }

        public VMgradeRulesEditor()
        {
            //if ( == null)
            //     = new RelayCommand();




            if (SelectedGarmentCommand == null)
                SelectedGarmentCommand = new RelayCommand(SelectedGarment);
            if (ChangeVertexCommand == null)
                ChangeVertexCommand = new RelayCommand(ChangeVertex);

            if (SaveXMLCommand == null)
                SaveXMLCommand = new RelayCommand(SaveToXML);
            
            if (CancelGradeRuleCommand == null)
                CancelGradeRuleCommand = new RelayCommand(CancelGradeRule);

            if (DeleteVertexCommand == null)
                DeleteVertexCommand = new RelayCommand(DeleteVertex);
            if (AddVertexCommand == null)
                AddVertexCommand = new RelayCommand(AddVertex);
            if (DeletePieceCommand == null)
                DeletePieceCommand = new RelayCommand(DeletePiece);
            if (AddPieceCommand == null)
                AddPieceCommand = new RelayCommand(AddPiece);
            if (DeleteMeasureCommand == null)
                DeleteMeasureCommand = new RelayCommand(DeleteMeasurement);
            if (AddMeasureCommand == null)
                AddMeasureCommand = new RelayCommand(AddMeasurement);
            if (AddStyleCommand == null)
                AddStyleCommand = new RelayCommand(AddStyle);

            LoadGradingRules();

            #region Consent Value


            //int _gradeID, int _vertexID, string _vertexName, Double _gradeDX = 0.0D, Double _gradeDY = 0.0D,
            //Vertex sd = new Vertex(), sf = new Vertex();

            //GradingRule tr = new GradingRule(10, 20, "Aa", .26, 0.45, sd, sf);
            //Vertex yt = new Vertex(tr);



            //Vertex vr1 = new Vertex(3, "XX");
            //Vertex vr2 = new Vertex(4, "YY");
            //Vertex vr3 = new Vertex(5, "ZZ");
            //Vertex vr4 = new Vertex(6, "PPgfjfgjf");
            //Vertex vr5 = new Vertex(7, "QQ");
            //Vertex vr6 = new Vertex(8, "RR");

            //GradingRule newGR1 = new GradingRule(9, 10, "ghjgfjAA", 0.5, 0.6, vr1, vr2);
            //GradingRule newGR2 = new GradingRule(11, 12, "BB", 0.7, -0.56, vr5, vr6);
            //GradingRule newGR3 = new GradingRule(newGR1);
            //GradingRule newGR4 = new GradingRule(newGR1);
            //GradingRules listGR = new GradingRules
            //{
            //    newGR1,
            //    newGR2
            //};
            //GradingRules listGR1 = new GradingRules
            //{
            //    newGR4,
            //    newGR3
            //};

            //Piece TempPiece3 = new Piece(1, "Cuff", listGR);
            //Piece TempPiece4 = new Piece(1, "Front", listGR1);
            //Pieces listPM = new Pieces
            //{
            //    TempPiece3,
            //    TempPiece4
            //};
            //DateTime trd = new DateTime();
            //trd = DateTime.Now;
            //Measurement TempMeasure1 = new Measurement(1, "Bust");
            //Measurement TempMeasure2 = new Measurement(1, "Whist");
            //Measurements listPG = new Measurements
            //{
            //    TempMeasure1,
            //    TempMeasure2
            //};

            //Garment TempGarment = new Garment(1, "Shirt", "yehud", null, trd);
            //GradingRulesTree.Add(TempGarment);

            #endregion
        }

        private void LoadGradingRules()
        {
            try
            {
                if (Properties.Settings.Default.GradeRulesXmlPath != string.Empty)
                    MyXmlSerializer.DeserializeObject(ref gradingRulesTree, Properties.Settings.Default.GradeRulesXmlPath);
                if (Properties.Settings.Default.StylesXmlPath != string.Empty)
                    MyXmlSerializer.DeserializeObject(ref garmentList, Properties.Settings.Default.StylesXmlPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }

        }

        private void AddStyle(object sender)
        {
            try
            {
                DialogLoadGarment dlgLoadGarment = new DialogLoadGarment();
                bool? result = dlgLoadGarment.ShowDialog();

                if (result.Value)
                {
                    GarmentInfo newGarment = dlgLoadGarment.GetNewGarment();
                    if (newGarment != null)
                    {
                        if (GarmentList.Count > 0)
                        {
                            var resultSearchGarment = GarmentList.Any(garmentCollection => garmentCollection.GarmentName == newGarment.GarmentName);

                            if (resultSearchGarment)
                            {
                                MessageBox.Show("This Style is already exist");
                                return;
                            }
                        }
                        //GradingRulesTree.Add(new Garment(garment));
                        // newGarment.MeasureCollection = dlgLoadGarment.GetNewMeasurements();
                        // newGarment.RuleCollection = dlgLoadGarment.GetRulesLibraries();
                        GarmentList.Add(new GarmentInfo(newGarment));
                    }
                    dlgLoadGarment.Close();
                    GarmentSelected = GarmentList.Last();
                    SelectedGarment(GarmentSelected);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void SelectedGarment(object sender)
        {
            try
            {
                GarmentToShow.Clear();
                if (GradingRulesTree.Any(germent => germent.GarmentName == GarmentSelected.GarmentName))
                    GarmentToShow.Add(GradingRulesTree.First(germent => germent.GarmentName == GarmentSelected.GarmentName));
                else
                {                   
                    GradingRulesTree.Add(new Garment (GarmentSelected));
                    GarmentToShow.Add(GradingRulesTree.First(germent => germent.GarmentName == GarmentSelected.GarmentName));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void DeleteStyle(object sender)
        {

        }

        private void AddMeasurement(object sender)
        {
            try
            {
              //  var elip = sender as Ellipse;
                var elip = sender as Image;
                ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(elip)));
                Garment parent = (Garment)presenter.Content;
                GarmentInfo findGarment = GarmentList.FirstOrDefault(garmentCollection => garmentCollection.GarmentName == parent.GarmentName);

                Measurements CustoMeasureColl = new Measurements();
                foreach (var item in findGarment.MeasureCollection)
                {
                    var searchMeasure = parent.MeasureCollection.Any(measureCollection => measureCollection.MeasureName == item.MeasureName);
                    if (searchMeasure == false)
                    {
                        CustoMeasureColl.Add(new Measurement(item));
                    }

                }
                DialogAddMeasure dlgAddMeasure = new DialogAddMeasure(CustoMeasureColl);
                bool? result = dlgAddMeasure.ShowDialog();

                if (result.Value)
                {
                    Measurement child = new Measurement(dlgAddMeasure.MeasureSelected());
                    
                    if (child != null)
                    {
                        child.MeasureIsDef = true;
                        child.ParentGarment = new GarmentDetails(parent.GarmentID, parent.GarmentName);
                        parent.MeasureCollection.Add(new Measurement(child));
                      //  findGarment.MeasureCollection.Remove(dlgAddMeasure.MeasureSelected());
                    }
                }
                else
                {
                    // MessageBox.Show("Do not chosen measurement");
                }
                dlgAddMeasure.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void DeleteMeasurement(object sender)
        {
            try
            {
               // var rect = sender as Rectangle;
                var rect = sender as Image;
                ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(rect));
                Measurement current = (Measurement)presenter.Content;
                //  bool emailExists = GradingRulesTree.Any(x => x.MeasureCollection.Any(y => y.MeasureName == parent.MeasureName));
                //  var findMeasure = (Measurements)GradingRulesTree.SelectMany(garmentCollection => garmentCollection.MeasureCollection.Where(measureCollection => measureCollection.MeasureName == parent.MeasureName));


                foreach (var item in GradingRulesTree)
                {
                    var findMeasure = item.MeasureCollection.FirstOrDefault(measureCollection => measureCollection.MeasureName == current.MeasureName
                    && measureCollection.ParentGarment.GarmentName == current.ParentGarment.GarmentName);
                    if (findMeasure != null)
                    {
                        findMeasure.ParentGarment = null;
                        item.MeasureCollection.Remove(findMeasure);
                        return;
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void AddPiece(object sender)
        {
            try
            {
                //var elip = sender as Ellipse;
                var elip = sender as Image;
                ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(elip));
                Measurement parent = (Measurement)presenter.Content;
                var findGarment = GarmentList.FirstOrDefault(garmentCollection => garmentCollection.GarmentName == parent.ParentGarment.GarmentName);
                RulesLibraries CustoRuleColl = new RulesLibraries();
                foreach (var item in findGarment.RuleCollection)
                {
                    var resultSearchPiece = parent.PieceCollection.Any(pieceCollection => pieceCollection.PieceName == item.MyPiece.PieceName);
                    if (resultSearchPiece == false)
                        CustoRuleColl.Add(new RuleLibrary(item));
                }
                DialogAddPiece dlgAddPiece = new DialogAddPiece(CustoRuleColl);
                bool? result = dlgAddPiece.ShowDialog();
                if (result.Value)
                {
                    Piece child = new Piece(dlgAddPiece.PieceSelected());
                    if (child != null)
                    {
                        child.ParentMeasurement = new Measurement(parent.MeasureID, parent.MeasureName);
                        child.ParentMeasurement.ParentGarment = new GarmentDetails(parent.ParentGarment.GarmentID, parent.ParentGarment.GarmentName);
                        parent.PieceCollection.Add(child);
                    }
                }
                else
                {
                   // MessageBox.Show("Not chosen piece");
                }
                dlgAddPiece.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void DeletePiece(object sender)
        {
            try
            {
               // var rect = sender as Rectangle;
                var rect = sender as Image;
                ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(rect));
                Piece current = (Piece)presenter.Content;

                foreach (var itemGarment in GradingRulesTree)
                {
                    foreach (var itemMeasure in itemGarment.MeasureCollection)
                    {
                        var findPiece = itemMeasure.PieceCollection.FirstOrDefault(pieceCollection => pieceCollection.PieceName == current.PieceName
                        && pieceCollection.ParentMeasurement.MeasureName == current.ParentMeasurement.MeasureName
                        && pieceCollection.ParentMeasurement.ParentGarment.GarmentName == current.ParentMeasurement.ParentGarment.GarmentName);
                        if (findPiece != null)
                        {
                            findPiece.ParentMeasurement = null;
                            itemMeasure.PieceCollection.Remove(findPiece);
                            return;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void AddVertex(object sender)
        {
            try
            {
                //var elip = sender as Ellipse;
                var elip = sender as Image;
                ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(elip));
                Piece parent = (Piece)presenter.Content;

                var findGarment = GarmentList.FirstOrDefault(garmentCollection => garmentCollection.GarmentName == parent.ParentMeasurement.ParentGarment.GarmentName);

                RuleLibrary CustoRuleColl = new RuleLibrary();
                Vertices VertexCollection = new Vertices();

                foreach (var itemPiece in findGarment.RuleCollection)
                {
                    if (itemPiece.MyPiece.PieceName == parent.PieceName)
                    {
                        CustoRuleColl.MyPiece = new Piece(itemPiece.MyPiece);
                        foreach (var itemVertex in itemPiece.VertexCollection)
                        {
                            var resultSearchPF_PL = parent.GradingCollection.Any(gradeCollection => gradeCollection.VertexLast.VertexName == itemVertex.VertexName || gradeCollection.VertexFirst.VertexName == itemVertex.VertexName);
                            if (resultSearchPF_PL == false)
                                VertexCollection.Add(new Vertex(itemVertex));
                            var resultSearchVertex = parent.GradingCollection.Any(gradeCollection => gradeCollection.VertexName == itemVertex.VertexName); // resultSearchGarment
                            if (resultSearchVertex == false)
                                CustoRuleColl.VertexCollection.Add(new Vertex(itemVertex));
                        }
                    }
                }
                GradingRule child = null;
                Vertex vertex = null, vertexPF = null, vertexPL = null;
                DialogAddVertex dlgAddVertex = null;

                dlgAddVertex = new DialogAddVertex(CustoRuleColl.VertexCollection, VertexCollection);
                bool? result = dlgAddVertex.ShowDialog();
                if (result.Value)
                {
                    vertex = dlgAddVertex.SelectedVertex();
                    vertexPF = dlgAddVertex.SelectedVertexPF();
                    vertexPL = dlgAddVertex.SelectedVertexPL();
                }
                if (vertex != null)
                {
                    child = new GradingRule(vertex);
                    //child = new GradingRule()
                    //{
                    //    VertexName = vertex.VertexName
                    //};
                    if (vertexPF != null)
                    {
                        child.VertexFirst = new Vertex(vertexPF);
                    }
                    //else
                    //{
                    //    child.VertexFirst = new Vertex();
                    //}
                    if (vertexPL != null)
                    {
                        child.VertexLast = new Vertex(vertexPL);
                    }
                    //else
                    //{
                    //    child.VertexLast = new Vertex();
                    //}
                    //DeepCopyChild = new GradingRule(child)
                    //{
                    //    
                    //};
                    child.ParentPiece = new Piece(parent.PieceID, parent.PieceName);
                    child.ParentPiece.ParentMeasurement = new Measurement(parent.ParentMeasurement.MeasureID, parent.ParentMeasurement.MeasureName);
                    child.ParentPiece.ParentMeasurement.ParentGarment = new GarmentDetails(parent.ParentMeasurement.ParentGarment.GarmentID, parent.ParentMeasurement.ParentGarment.GarmentName);
                    parent.GradingCollection.Add(child);
                }
                else
                {
                    //  MessageBox.Show("Not chosen point");
                }
                dlgAddVertex.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void DeleteVertex(object sender)
        {
            try
            {
               // var rect = sender as Rectangle;
                var rect = sender as Image;
                ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(rect));
                GradingRule current = (GradingRule)presenter.Content;
                foreach (var itemGarment in GradingRulesTree)
                {
                    foreach (var itemMeasure in itemGarment.MeasureCollection)
                    {
                        foreach (var itemPiece in itemMeasure.PieceCollection)
                        {
                            var findVertex = itemPiece.GradingCollection.FirstOrDefault(vertexCollection => vertexCollection.VertexName == current.VertexName
                            && vertexCollection.ParentPiece.PieceName == current.ParentPiece.PieceName
                            && vertexCollection.ParentPiece.ParentMeasurement.MeasureName == current.ParentPiece.ParentMeasurement.MeasureName
                            && vertexCollection.ParentPiece.ParentMeasurement.ParentGarment.GarmentName == current.ParentPiece.ParentMeasurement.ParentGarment.GarmentName);
                            if (findVertex != null)
                            {
                                findVertex.ParentPiece = null;
                                itemPiece.GradingCollection.Remove(findVertex);
                                return;
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void ChangeVertex(object sender)
        {
            try
            {
                // ... Get control that raised this event.
                var textBox = sender as TextBox;




                ContentPresenter presenter = (ContentPresenter)VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(VisualTreeHelper.GetParent(textBox)));
                GradingRule current = (GradingRule)presenter.Content;
                foreach (var itemGarment in GradingRulesTree)
                {
                    foreach (var itemMeasure in itemGarment.MeasureCollection)
                    {
                        foreach (var itemPiece in itemMeasure.PieceCollection)
                        {
                            var findVertex = itemPiece.GradingCollection.FirstOrDefault(vertexCollection => vertexCollection.VertexName == current.VertexName
                            && vertexCollection.ParentPiece.PieceName == current.ParentPiece.PieceName
                            && vertexCollection.ParentPiece.ParentMeasurement.MeasureName == current.ParentPiece.ParentMeasurement.MeasureName
                            && vertexCollection.ParentPiece.ParentMeasurement.ParentGarment.GarmentName == current.ParentPiece.ParentMeasurement.ParentGarment.GarmentName);
                            if (findVertex != null)
                            {
                                if (findVertex.VertexFirst.VertexName == findVertex.VertexLast.VertexName)
                                {
                                    findVertex.VertexFirst = new Vertex();
                                    findVertex.VertexLast = new Vertex();
                                }

                                if (findVertex.VertexFirst.VertexName == findVertex.VertexName)
                                {
                                    findVertex.VertexFirst = new Vertex();
                                }
                                if (findVertex.VertexLast.VertexName == findVertex.VertexName)
                                {
                                    findVertex.VertexLast = new Vertex();
                                }
                                return;
                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void SaveToXML(object sender)
        {
            try
            {
                MyXmlSerializer.SerializeObject(GradingRulesTree, Properties.Settings.Default.GradeRulesXmlPath);
                MyXmlSerializer.SerializeObject(GarmentList, Properties.Settings.Default.StylesXmlPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void CancelGradeRule(object sender)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("Do you want to save changes ?", "Confirmation", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    SaveToXML(sender);
                    GarmentSelected = null;
                    GarmentToShow.Clear();
                }
                else if (result == MessageBoxResult.No)
                {
                    GarmentSelected = null;
                    GarmentToShow.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

    }
}
