﻿using Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using Views.Utilities;

namespace Views.ViewModels
{

    class VMordersMatchEditor : BaseModel
    {
        // unnecessary
        //private Measurements garmentMeasureLoad = null;
        //public Measurements GarmentMeasureLoad
        //{
        //    get => garmentMeasureLoad; set => garmentMeasureLoad = value;
        //}

        //3 list for find Base Size

        private RecommendSizes critiGrmMeasureCollection = null;
        public RecommendSizes CritiGrmMeasureCollection
        {
            get => critiGrmMeasureCollection; set => critiGrmMeasureCollection = value;
        }

        private CustomersMeasurement critiOrdMeasureCollection = null;
        public CustomersMeasurement CritiOrdMeasureCollection
        {
            get => critiOrdMeasureCollection; set => critiOrdMeasureCollection = value;
        }

        private ObservableCollection<string> gradingValues = null;
        public ObservableCollection<string> GradingValues
        {
            get => gradingValues; set => gradingValues = value;
        }

        
        // list for listView & Selected one order

        private OrdersDetails ordertList = null;
        public OrdersDetails OrderList
        {
            get
            {
                if (ordertList == null)
                    ordertList = new OrdersDetails();
                return ordertList;
            }
            set { SetPropertry(ref ordertList, value); }
        }

        private OrderDetails orderSelected;
        public OrderDetails OrderSelected
        {
            //get
            //{
            //    if (orderSelected == null)
            //        orderSelected = new OrderDetails();               
            //    return orderSelected;
            //}
            get
            { return orderSelected; }
            set { SetPropertry(ref orderSelected, value); }
        }


        // list for info - bath & path 

        private Garments gradingRulesTree = null; // OrdertList
        public Garments GradingRulesTree 
        {
            get
            {
                if (gradingRulesTree == null)
                    gradingRulesTree = new Garments();
                LoadGradingRules();
                return gradingRulesTree;
            }
            set { SetPropertry(ref gradingRulesTree, value); }
        }


        private GarmentsInfo garmentList = null;
        public GarmentsInfo GarmentList
        {
            get
            {
                if (garmentList == null)
                    garmentList = new GarmentsInfo();
                LoadStylesData();
                return garmentList;
            }
            set { SetPropertry(ref garmentList, value); }

        }

        //

        private RecommendSize recMeasureSelect;
        public RecommendSize RecMeasureSelect
        {
            get { return recMeasureSelect; }
            set { SetPropertry(ref recMeasureSelect, value); }
        }

        private Measurement grmMeasureSelect;
        public Measurement GrmMeasureSelect
        {
            get { return grmMeasureSelect; }
            set { SetPropertry(ref grmMeasureSelect, value); }
        }


        // unnecessary
        //private RecommendSizes grmntMeasureSelectedCollection = null;
        //public RecommendSizes GrmntMeasureSelectedCollection
        //{
        //    get
        //    {
        //        if (grmntMeasureSelectedCollection == null)
        //            grmntMeasureSelectedCollection = new RecommendSizes();
        //        return grmntMeasureSelectedCollection;
        //    }
        //    set { SetPropertry(ref grmntMeasureSelectedCollection, value); }
        //}
        // unnecessary
        //private Measurements grmntMeasureCollection = null;
        //public Measurements GrmntMeasureCollection
        //{
        //    get
        //    {
        //        if (grmntMeasureCollection == null)
        //            grmntMeasureCollection = new Measurements();
        //        return grmntMeasureCollection;
        //    }
        //    set { SetPropertry(ref grmntMeasureCollection, value); }
        //}

        //  // unnecessary

        //private CustomerMeasurement ordUnSelectedMeasure;
        //public CustomerMeasurement OrdUnSelectedMeasure
        //{
        //    //get
        //    //{
        //    //    if (ordUnSelectedMeasure == null)
        //    //        ordUnSelectedMeasure = new CustomerMeasurement();
        //    //    return ordUnSelectedMeasure;
        //    //}
        //    get { return ordUnSelectedMeasure; }
        //    set { SetPropertry(ref ordUnSelectedMeasure, value); }
        //}

        private CustomerMeasurement ordMeasureSelect;
        public CustomerMeasurement OrdMeasureSelect
        {
            get { return ordMeasureSelect; }
            set { SetPropertry(ref ordMeasureSelect, value); }
        }


        // unnecessary
        //private CustomersMeasurement ordMeasureSelectedCollection = null;
        //public CustomersMeasurement OrdMeasureSelectedCollection
        //{
        //    get
        //    {
        //        if (ordMeasureSelectedCollection == null)
        //            ordMeasureSelectedCollection = new CustomersMeasurement();
        //        return ordMeasureSelectedCollection;
        //    }
        //    set { SetPropertry(ref ordMeasureSelectedCollection, value); }
        //}
        //// unnecessary
        //private CustomersMeasurement ordMeasureCollection = null;
        //public CustomersMeasurement OrdMeasureCollection
        //{
        //    get
        //    {
        //        if (ordMeasureCollection == null)
        //            ordMeasureCollection = new CustomersMeasurement();
        //        return ordMeasureCollection;
        //    }
        //    set { SetPropertry(ref ordMeasureCollection, value); }
        //}

        //

        public RelayCommand AddOrderCommand { get; set; }
        public RelayCommand SelectedOrderCommand { get; set; }
        public RelayCommand DetermineRecommendSizeCommand { get; set; }
        public RelayCommand GenerateBatchFileCommand { get; set; }

        //

        public RelayCommand CellChangedCommand { get; set; }

        public RelayCommand SelectMeasureFromOrderCommand { get; set; }
        public RelayCommand UnSelectMeasureFromOrderCommand { get; set; }

        public RelayCommand SelectMeasureFromGarmentCommand { get; set; }
        public RelayCommand UnSelectMeasureFromGarmentCommand { get; set; }

        public RelayCommand GarmentMoveUpCommand { get; set; } 
        public RelayCommand GarmentMoveDownCommand { get; set; } 

        public RelayCommand OrdMoveUpCommand { get; set; }
        public RelayCommand OrdMoveDownCommand { get; set; }

        //
        public RelayCommand CancelMatchEditorCommand { get; set; }
        public RelayCommand RunProcessCommand { get; set; }
        //

        public VMordersMatchEditor()
        {

            //OrderSelected.PropertyChanged += SelectedItem_PropertyChanged;
            //OrdUnSelectedMeasure.PropertyChanged += SelectedItem_PropertyChanged;
            if (RunProcessCommand == null)
                RunProcessCommand = new RelayCommand(RunOptitex);
            if (CancelMatchEditorCommand == null)
                CancelMatchEditorCommand = new RelayCommand(CancelMatchEditor);
            if (CellChangedCommand == null)
                CellChangedCommand = new RelayCommand(Change);
            if (OrdMoveDownCommand == null)
                OrdMoveDownCommand = new RelayCommand(MoveDownOrdMeasure);
            if (OrdMoveUpCommand == null)
                OrdMoveUpCommand = new RelayCommand(MoveUpOrdMeasure);
            if (GarmentMoveDownCommand == null)
                GarmentMoveDownCommand = new RelayCommand(MoveDownGrmMeasure);
            if (GarmentMoveUpCommand == null)
                GarmentMoveUpCommand = new RelayCommand(MoveUpGrmMeasure);
            if (UnSelectMeasureFromGarmentCommand == null)
                UnSelectMeasureFromGarmentCommand = new RelayCommand(UnSelectGarmentMeasure);
            if (SelectMeasureFromGarmentCommand == null)
                SelectMeasureFromGarmentCommand = new RelayCommand(SelectGarmentMeasure);
            if (UnSelectMeasureFromOrderCommand == null)
                UnSelectMeasureFromOrderCommand = new RelayCommand(UnSelectOrderMeasure);
            if (SelectMeasureFromOrderCommand == null)
                SelectMeasureFromOrderCommand = new RelayCommand(SelectOrderMeasure);
            if (GenerateBatchFileCommand == null)
                GenerateBatchFileCommand = new RelayCommand(GenerateBatchFile);
            if (DetermineRecommendSizeCommand == null)
                DetermineRecommendSizeCommand = new RelayCommand(DetermineRecommendSize);
            if (SelectedOrderCommand == null)
                SelectedOrderCommand = new RelayCommand(SelectedOrder);
            if (AddOrderCommand == null)
                AddOrderCommand = new RelayCommand(AddOrder);
            LoadGradingRules();
            LoadStylesData();
        }

        private void LoadStylesData()
        {
            try
            {
                if (Properties.Settings.Default.StylesXmlPath != string.Empty)
                    MyXmlSerializer.DeserializeObject(ref garmentList, Properties.Settings.Default.StylesXmlPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void LoadGradingRules()
        {
            try
            {
                if (Properties.Settings.Default.GradeRulesXmlPath != string.Empty)
                    MyXmlSerializer.DeserializeObject(ref gradingRulesTree, Properties.Settings.Default.GradeRulesXmlPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void AddOrder(object sender)
        {
            try
            {
                DialogLoadOrder dlgLoadOrder = new DialogLoadOrder(GradingRulesTree);
                bool? result = dlgLoadOrder.ShowDialog();

                if (result.Value)
                {
                    Garment garmentSelect = dlgLoadOrder.GarmentSelected();
                    var findGarment = GarmentList.FirstOrDefault(garmentCollection => garmentCollection.GarmentName == garmentSelect.GarmentName);
                    OrderDetails orderDetail = new OrderDetails(dlgLoadOrder.GetNewOrder())
                    {
                        OrderGarment = new GarmentInfo(garmentSelect.GarmentID, garmentSelect.GarmentName, findGarment.GarmentPath, findGarment.DMCPath),
                        GrmMeasureCollection = LoadDMC(findGarment.DMCPath)
                    };
                    

                    if (orderDetail != null)
                    {
                        if (OrderList.Count > 0)
                        {
                            var resultSearchGarment = OrderList.Any(orderCollection => orderCollection.OrderID == orderDetail.OrderID);

                            if (resultSearchGarment)
                            {
                                MessageBox.Show("This Order is already exist");
                                return;
                            }
                        }
                        
                        // orderDetail.OrderGarment.GarmentPath = findGarment.GarmentPath;
                        // orderDetail.OrderGarment.DMCPath = findGarment.DMCPath;
                        // LoadDMC(findGarment.DMCPath)
                        //foreach (Measurement item in GarmentMeasureLoad)
                        //{
                        //    var searchMeasure = orderDetail.OrderGarment.MeasureCollection.Any(measure => measure.MeasureName == item.MeasureName);
                        //    if (searchMeasure == false)
                        //    {
                        //        orderDetail.OrderGarment.MeasureCollection.Add(new Measurement(item));
                        //    }
                        //}



                        OrderList.Add(new OrderDetails(orderDetail));
                    }
                    dlgLoadOrder.Close();
                    OrderSelected = OrderList.Last();
                    SelectedOrder(OrderSelected);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private Measurements LoadDMC(string _path)
        {
            try
            {
                ExcelParserClass mAppExcel = new ExcelParserClass();
                Measurements measureCollectoin = new Measurements();
                mAppExcel.OpenExcelFile(_path, 1);

                if (mAppExcel.excelSheet != null && mAppExcel.excelRange != null)
                {
                    // Get a reference to the Garment Measurement collection.
                    // and add it to the  ObservableCollection.
                    mAppExcel.ImportMeasure(ref measureCollectoin, mAppExcel.excelRange);
                }
                return measureCollectoin;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
                return null;
            }

        }

        private void SelectedOrder(object sender)
        {
            //try
            //{

            //    if (OrderList.Any(order => order.OrderID == OrderSelected.OrderID))

            //    {
            //        OrdMeasureCollection.Clear();                    
            //        OrdMeasureSelectedCollection.Clear();
            //        GrmntMeasureCollection.Clear();
            //        GrmntMeasureSelectedCollection.Clear();

            //        OrdMeasureCollection = OrderSelected.CstmrMeasureCollection;
            //        OrdMeasureSelectedCollection = OrderSelected.CstmrMeasureSelectedCollec;
            //        GrmntMeasureCollection = OrderSelected.OrderGarment.MeasureCollection;
            //        GrmntMeasureSelectedCollection = OrderSelected.GrmMeasureSelectedCollec;
            //    }               
            //    else
            //    {                   


            //        OrdMeasureCollection = new CustomersMeasurement();
            //        foreach (CustomerMeasurement item in OrderSelected.CstmrMeasureCollection)
            //        {
            //            CustomerMeasurement newItem = new CustomerMeasurement(item);
            //            OrdMeasureCollection.Add(newItem);
            //        }
            //        OrdMeasureSelectedCollection = new CustomersMeasurement();
            //        foreach (CustomerMeasurement item in OrderSelected.CstmrMeasureSelectedCollec)
            //        {
            //            CustomerMeasurement newItem = new CustomerMeasurement(item);
            //            OrdMeasureSelectedCollection.Add(newItem);
            //        }



            //        //
            //        GrmntMeasureCollection = new Measurements();
            //        foreach (Measurement item in OrderSelected.OrderGarment.MeasureCollection)
            //        {
            //            Measurement newItem = new Measurement(item);
            //            GrmntMeasureCollection.Add(newItem);
            //        }
            //        GrmntMeasureSelectedCollection = new RecommendSizes();
            //        foreach (RecommendSize item in OrderSelected.GrmMeasureSelectedCollec)
            //        {
            //            RecommendSize newItem = new RecommendSize(item);
            //            GrmntMeasureSelectedCollection.Add(newItem);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, ex.Source);
            //}
        }

        private void SelectOrderMeasure(object sender)
        {
            try
            {
                if (OrdMeasureSelect != null)
                {
                    //CustomerMeasurement findMeasure = OrdMeasureCollection.FirstOrDefault(measure => measure.CstmrMeasureName == OrdMeasureSelect.CstmrMeasureName);
                    //OrdMeasureSelectedCollection.Add(new CustomerMeasurement(findMeasure));
                    //OrderSelected.CstmrMeasureSelectedCollec.Add(new CustomerMeasurement(findMeasure));
                    //OrdMeasureCollection.Remove(findMeasure);
                    //OrderSelected.CstmrMeasureCollection.Remove(findMeasure);
                    //OrdMeasureSelect = null;


                    CustomerMeasurement findMeasure = OrderSelected.CstmrMeasureCollection.FirstOrDefault(measure => measure.CstmrMeasureName == OrdMeasureSelect.CstmrMeasureName);
                    OrderSelected.CstmrMeasureSelectedCollec.Add(new CustomerMeasurement(findMeasure));
                    OrderSelected.CstmrMeasureCollection.Remove(findMeasure);
                    OrdMeasureSelect = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void UnSelectOrderMeasure(object sender)
        {
            try
            {
                if (OrdMeasureSelect != null)
                {
                    //CustomerMeasurement findMeasure = OrdMeasureSelectedCollection.FirstOrDefault(measure => measure.CstmrMeasureName == OrdUnSelectedMeasure.CstmrMeasureName);
                    //OrdMeasureCollection.Add(new CustomerMeasurement(findMeasure));
                    //OrderSelected.CstmrMeasureCollection.Add(new CustomerMeasurement(findMeasure));
                    //OrdMeasureSelectedCollection.Remove(findMeasure);
                    //OrderSelected.CstmrMeasureSelectedCollec.Remove(findMeasure);
                    //OrdMeasureSelect = null;



                    CustomerMeasurement findMeasure = OrderSelected.CstmrMeasureSelectedCollec.FirstOrDefault(measure => measure.CstmrMeasureName == OrdMeasureSelect.CstmrMeasureName);                  
                    OrderSelected.CstmrMeasureCollection.Add(new CustomerMeasurement(findMeasure));                   
                    OrderSelected.CstmrMeasureSelectedCollec.Remove(findMeasure);
                    OrdMeasureSelect = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void SelectGarmentMeasure(object sender)
        {
            try
            {
                if (GrmMeasureSelect != null)
                {
                    //Measurement findMeasure = GrmntMeasureCollection.FirstOrDefault(measure => measure.MeasureName == GrmMeasureSelect.MeasureName);
                    //GrmntMeasureSelectedCollection.Add(new RecommendSize(findMeasure));
                    //OrderSelected.GrmMeasureSelectedCollection.Add(new RecommendSize(findMeasure));
                    //GrmntMeasureCollection.Remove(findMeasure);
                    //OrderSelected.OrderGarment.MeasureCollection.Remove(findMeasure);
                    //GrmMeasureSelect = null;



                    Measurement findMeasure = OrderSelected.GrmMeasureCollection.FirstOrDefault(measure => measure.MeasureName == GrmMeasureSelect.MeasureName);                    
                    OrderSelected.GrmMeasureSelectedCollec.Add(new RecommendSize(findMeasure));                   
                    OrderSelected.GrmMeasureCollection.Remove(findMeasure);
                    GrmMeasureSelect = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void UnSelectGarmentMeasure(object sender)
        {
            try
            {
                if (RecMeasureSelect != null)
                {
                    //RecommendSize findMeasure = GrmntMeasureSelectedCollection.FirstOrDefault(measure => measure.MeasureName == RecMeasureSelect.MeasureName);
                    //GrmntMeasureCollection.Add(new Measurement(findMeasure));
                    //OrderSelected.OrderGarment.MeasureCollection.Add(new Measurement(findMeasure));
                    //GrmntMeasureSelectedCollection.Remove(findMeasure);
                    //OrderSelected.GrmMeasureSelectedCollection.Remove(findMeasure);
                    //RecMeasureSelect = null;


                    RecommendSize findMeasure = OrderSelected.GrmMeasureSelectedCollec.FirstOrDefault(measure => measure.MeasureName == RecMeasureSelect.MeasureName);
                    OrderSelected.GrmMeasureCollection.Add(new Measurement(findMeasure));
                    OrderSelected.GrmMeasureSelectedCollec.Remove(findMeasure);
                    RecMeasureSelect = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }


        private void MoveUpGrmMeasure(object sender) 
        {
            RecommendSize garmentMeasure = ((FrameworkElement)sender).DataContext as RecommendSize;
            //int sel = GrmntMeasureSelectedCollection.IndexOf(garmentMeasure);
            //if (sel > 0)
            //{
            //    GrmntMeasureSelectedCollection.RemoveAt(sel);
            //    GrmntMeasureSelectedCollection.Insert(sel - 1, garmentMeasure);
            //}


            int sel = OrderSelected.GrmMeasureSelectedCollec.IndexOf(garmentMeasure);
            if (sel > 0)
            {
                OrderSelected.GrmMeasureSelectedCollec.RemoveAt(sel);
                OrderSelected.GrmMeasureSelectedCollec.Insert(sel - 1, garmentMeasure);
            }
        }

        private void MoveDownGrmMeasure(object sender)
        {
            RecommendSize garmentMeasure = ((FrameworkElement)sender).DataContext as RecommendSize;
            //int sel = GrmntMeasureSelectedCollection.IndexOf(garmentMeasure);
            //if (sel < GrmntMeasureSelectedCollection.Count - 1)
            //{
            //    GrmntMeasureSelectedCollection.RemoveAt(sel);
            //    GrmntMeasureSelectedCollection.Insert(sel + 1, garmentMeasure);
            //}


            int sel = OrderSelected.GrmMeasureSelectedCollec.IndexOf(garmentMeasure);
            if (sel < OrderSelected.GrmMeasureSelectedCollec.Count - 1)
            {
                OrderSelected.GrmMeasureSelectedCollec.RemoveAt(sel);
                OrderSelected.GrmMeasureSelectedCollec.Insert(sel + 1, garmentMeasure);
            }
        }

        private void MoveUpOrdMeasure(object sender)
        {

            if ((sender as FrameworkElement).DataContext is CustomerMeasurement orderMeasure)
            {
                //int sel = OrdMeasureSelectedCollection.IndexOf(orderMeasure);
                //if (sel > 0)
                //{
                //    OrdMeasureSelectedCollection.RemoveAt(sel);
                //    OrdMeasureSelectedCollection.Insert(sel - 1, orderMeasure);
                //}

                int sel = OrderSelected.CstmrMeasureSelectedCollec.IndexOf(orderMeasure);
                if (sel > 0)
                {
                    OrderSelected.CstmrMeasureSelectedCollec.RemoveAt(sel);
                    OrderSelected.CstmrMeasureSelectedCollec.Insert(sel - 1, orderMeasure);
                }
            }
        }

        private void MoveDownOrdMeasure(object sender)
        {
            if (((FrameworkElement)sender).DataContext is CustomerMeasurement orderMeasure)
            {
                //int sel = OrdMeasureSelectedCollection.IndexOf(orderMeasure);
                //if (sel < OrdMeasureSelectedCollection.Count - 1)
                //{
                //    OrdMeasureSelectedCollection.RemoveAt(sel);
                //    OrdMeasureSelectedCollection.Insert(sel + 1, orderMeasure);
                //}

                int sel = OrderSelected.CstmrMeasureSelectedCollec.IndexOf(orderMeasure);
                if (sel < OrderSelected.CstmrMeasureSelectedCollec.Count - 1)
                {
                    OrderSelected.CstmrMeasureSelectedCollec.RemoveAt(sel);
                    OrderSelected.CstmrMeasureSelectedCollec.Insert(sel + 1, orderMeasure);
                }
            }
        } 

        private void Change(object sender)
        {
            if ((sender) is CustomerMeasurement orderMeasure)
            {
                string sel = orderMeasure.CstmrMeasureValue;

            }
        }

        private void SelectedItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // this will be called when any property value of the SelectedItem object changes
            //if (e.PropertyName == "YourPropertyName") DoSomethingHere();
            //else if (e.PropertyName == "OtherPropertyName") DoSomethingElse();
        }


        private void DetermineRecommendSize(object sender)
        {
            try
            {
                if (OrderSelected.OrderBaseSize < 0)
                {
                    //int orderMeasureNum = OrdMeasureSelectedCollection.Count,
                    //garmentMeasureNum = GrmntMeasureSelectedCollection.Count;

                    int orderMeasureNum = OrderSelected.CstmrMeasureSelectedCollec.Count,
                   garmentMeasureNum = OrderSelected.GrmMeasureSelectedCollec.Count;

                    if (orderMeasureNum != garmentMeasureNum || orderMeasureNum == 0)
                    {
                        MessageBox.Show("The number of measurements does not match");
                        return;
                    }
                    foreach (var item in OrderSelected.CstmrMeasureSelectedCollec)
                    {
                        // CustomerMeasurement orderMeasure = item as CustomerMeasurement;

                        bool isNumeric = double.TryParse(item.CstmrMeasureValue, out double n);
                        if (!isNumeric)
                        {
                            MessageBox.Show("The measurement from order contains error value");
                            return;
                        }
                    }
                    bool critiHighChosen = false;
                    CritiOrdMeasureCollection = new CustomersMeasurement();
                    CritiGrmMeasureCollection = new RecommendSizes();
                    int index = 0;
                    var findMeasureHigh = OrderSelected.CstmrMeasureSelectedCollec.FirstOrDefault(measure => measure.CstmrMeasureName == Properties.Settings.Default.criticalMeasureHigh);
                    if (findMeasureHigh != null)
                    {
                        CritiOrdMeasureCollection.Add(new CustomerMeasurement(findMeasureHigh));
                        index = OrderSelected.CstmrMeasureSelectedCollec.IndexOf(findMeasureHigh);
                        CritiGrmMeasureCollection.Add(OrderSelected.GrmMeasureSelectedCollec[index]);
                        critiHighChosen = true;
                    }
                    var findMeasureMedium = OrderSelected.CstmrMeasureSelectedCollec.FirstOrDefault(measure => measure.CstmrMeasureName == Properties.Settings.Default.criticalMeasureMedium);
                    if (findMeasureMedium != null)
                    {
                        CritiOrdMeasureCollection.Add(findMeasureMedium);
                        index = OrderSelected.CstmrMeasureSelectedCollec.IndexOf(findMeasureMedium);
                        CritiGrmMeasureCollection.Add(OrderSelected.GrmMeasureSelectedCollec[index]);
                    }
                    var findMeasureLow = OrderSelected.CstmrMeasureSelectedCollec.FirstOrDefault(measure => measure.CstmrMeasureName == Properties.Settings.Default.criticalMeasureLow);
                    if (findMeasureLow != null)
                    {
                        CritiOrdMeasureCollection.Add(findMeasureLow);
                        index = OrderSelected.CstmrMeasureSelectedCollec.IndexOf(findMeasureLow);
                        CritiGrmMeasureCollection.Add(OrderSelected.GrmMeasureSelectedCollec[index]);
                    }

                    if (CritiOrdMeasureCollection.Count == 0 || !critiHighChosen)
                    {
                        MessageBox.Show("Critical measure " + Properties.Settings.Default.criticalMeasureHigh + " is not selected");
                        return;
                    }
                    if (critiHighChosen)
                    {
                        ExcelParserClass mAppExcel = new ExcelParserClass();
                        mAppExcel.OpenExcelFile(OrderSelected.OrderGarment.DMCPath, 1);
                        mAppExcel.ImportSizeRange(ref gradingValues, mAppExcel.excelRange);
                        mAppExcel = new ExcelParserClass();
                        mAppExcel.OpenExcelFile(OrderSelected.OrderGarment.DMCPath, 1);
                        OrderSelected.OrderBaseSize = mAppExcel.FindRecommendedZise(gradingValues.Count, CritiGrmMeasureCollection, CritiOrdMeasureCollection, mAppExcel.excelRange);
                    }
                    if (OrderSelected.OrderBaseSize >= 0)
                    {
                        OrderSelected.RecommendSize = GradingValues[OrderSelected.OrderBaseSize - 3];
                        CompereDelta();
                    }
                }
                //else // if (OrderSelected.OrderBaseSize >= 0)
                //{
                //    string str = GradingValues[OrderSelected.OrderBaseSize - 3];
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void CompereDelta()
        {
            double OrdMeasureValue = 0.0, different = 0.0;
            try
            {
                ExcelParserClass mAppExcel = new ExcelParserClass();
                mAppExcel.OpenExcelFile(OrderSelected.OrderGarment.DMCPath, 1);
                RecommendSizes measureCollectoin = new RecommendSizes();
                measureCollectoin =  OrderSelected.GrmMeasureSelectedCollec;
                mAppExcel.ImportRecommendedZise(ref measureCollectoin, OrderSelected.OrderBaseSize, mAppExcel.excelRange);
                

                for (int i = 0; i < OrderSelected.GrmMeasureSelectedCollec.Count; i++)
                {                   
                    OrdMeasureValue = Convert.ToDouble(OrderSelected.CstmrMeasureSelectedCollec[i].CstmrMeasureValue); //
                    different = (OrdMeasureValue - OrderSelected.GrmMeasureSelectedCollec[i].RcmMeasureValue);
                    OrderSelected.GrmMeasureSelectedCollec[i].Delta = (different > 0) ? different.ToString("#.00") : different.ToString("#.00");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }

        }

        private void GenerateBatchFile(object sender)
        {
            try
            {
                var findGarment = GradingRulesTree.FirstOrDefault(garmentCollection => garmentCollection.GarmentName == OrderSelected.OrderGarment.GarmentName);
                if (findGarment != null && OrderSelected.OrderBaseSize >= 0) // //&& OrderSelected.OrderStage == Status.ready
                {
                    var orderDirectoryPath = Path.GetDirectoryName(OrderSelected.OrderPath);
                    var garmentDirectoryPath = Path.GetDirectoryName(OrderSelected.OrderGarment.GarmentPath); // 
                    var garmentfileNameWithExt = Path.GetFileNameWithoutExtension(OrderSelected.OrderGarment.GarmentPath) + "_new.PDS";
                    string newPDS = Path.Combine(garmentDirectoryPath, garmentfileNameWithExt);



                    string fileNameWithExt = OrderSelected.OrderGarment.GarmentName + "_" + Properties.Settings.Default.batchFileName;
                    // Use Combine again to add the file name to the path.
                    string pathString = Path.Combine(orderDirectoryPath, fileNameWithExt),
                    newLineComment = ";;", text = ";;---" + OrderSelected.OrderGarment.GarmentName + "---;;";
                    OrderSelected.BatchPath = pathString;
                    using (StreamWriter file = new StreamWriter(pathString))
                    {
                        file.WriteLine(text);
                        file.WriteLine(newLineComment);
                        text = "@UNIT /" + OrderSelected.OrderUnits;
                        file.WriteLine(text);
                        text = "@NEW";
                        file.WriteLine(text);
                        text = "@OPEN /FILE=" + OrderSelected.OrderGarment.GarmentPath;
                        file.WriteLine(text);
                        text = "@BASESIZE /SIZE=" + OrderSelected.RecommendSize;
                        file.WriteLine(text);
                        file.WriteLine(newLineComment);

                       

                        for (int k = 0; k < OrderSelected.GrmMeasureSelectedCollec.Count; k++)
                        {
                            var selected = findGarment.MeasureCollection[k];
                            var val = Convert.ToDouble(OrderSelected.GrmMeasureSelectedCollec.FirstOrDefault(x => x.MeasureName == selected.MeasureName).Delta);
                            text = ";;;;" + findGarment.MeasureCollection[k].MeasureName + ";;;;";
                            file.WriteLine(text);
                            file.WriteLine(newLineComment);
                            for (int l = 0; l < findGarment.MeasureCollection[k].PieceCollection.Count; l++)
                            {
                                text = ";;" + findGarment.MeasureCollection[k].PieceCollection[l].PieceName + ";;";
                                file.WriteLine(text);
                                file.WriteLine(newLineComment);
                                for (int m = 0; m < findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection.Count; m++)
                                {
                                    text = "@MTM /PNT=" + findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection[m].VertexName;
                                    if (findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection[m].GradeDX != 0)
                                    {
                                        text += " /DX=" + ((findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection[m].GradeDX / 100) * val).ToString();
                                    }
                                    if (findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection[m].GradeDY != 0)
                                    {
                                        text += " /DY=" + ((findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection[m].GradeDY / 100) * val).ToString();
                                    }
                                    if (!string.IsNullOrWhiteSpace(findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection[m].VertexFirst.VertexName))
                                    {
                                        text += " /FRSFIX=" + findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection[m].VertexFirst.VertexName;
                                    }
                                    if (!string.IsNullOrWhiteSpace(findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection[m].VertexLast.VertexName))
                                    {
                                        text += " /LSFIX=" + findGarment.MeasureCollection[k].PieceCollection[l].GradingCollection[m].VertexLast.VertexName;
                                    }
                                    file.WriteLine(text);
                                    file.WriteLine(newLineComment);
                                }
                            }
                        }
                        file.WriteLine(newLineComment);
                        text = "@SAVE /FILE=" + newPDS;
                        file.WriteLine(text);
                        //text = "@EXIT";
                        //file.WriteLine(text);
                    }
                }
                else
                {
                    MessageBox.Show("Is not exist a grading rules");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }

        }


        private void CancelMatchEditor(object sender)
        {
            try
            {
                MessageBoxResult result = MessageBox.Show("Do you want to save changes ?", "Confirmation", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    // SaveToXML(sender); 
                    OrderSelected = null;
                    //GrmntMeasureSelectedCollection.Clear();
                    //GrmntMeasureCollection.Clear();
                    //OrdMeasureSelectedCollection.Clear();
                    //OrdMeasureCollection.Clear();
                }
                else if (result == MessageBoxResult.No)
                {
                    OrderSelected = null;
                    //GrmntMeasureSelectedCollection.Clear();
                    //GrmntMeasureCollection.Clear();
                    //OrdMeasureSelectedCollection.Clear();
                    //OrdMeasureCollection.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source);
            }
        }

        private void RunOptitex(object sender)
        {
            if (OrderSelected != null)
            {
                if (File.Exists(OrderSelected.BatchPath) && (Properties.Settings.Default.OptitexAppPath != ""))
                    Process.Start(Properties.Settings.Default.OptitexAppPath, OrderSelected.BatchPath);
                //  Process.Start("C:\\Program Files\\Optitex 15\\App\\PDS15.exe", OrderSelected.BatchPath); //
            }



            //if (OrderSelected.BatchPath != null)
            //    Process.Start(OrderSelected.BatchPath);

            //   // Prepare the process to run
            //   ProcessStartInfo start = new ProcessStartInfo();
            //   // Enter in the command line arguments, everything you would enter after the executable name itself
            ////   start.Arguments = arguments;
            //   // Enter the executable to run, including the complete path
            ////   start.FileName = ExeName;
            //   // Do you want to show a console window?
            //   start.WindowStyle = ProcessWindowStyle.Hidden;
            //   start.CreateNoWindow = true;
            //   int exitCode;


            //   // Run the external process & wait for it to finish
            //   using (Process proc = Process.Start("C:\\Program Files\\Optitex 15\\App\\PDS15.exe")) // C:\Users\yehudak\Desktop\seltex\Batch_File_rules.btf
            //   {
            //       proc.WaitForExit();

            //       // Retrieve the app's exit code
            //       exitCode = proc.ExitCode;
            //   }


            // //  Process.Start("CMD.exe", blabla);
        }


    }

}
